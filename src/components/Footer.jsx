import React from 'react'

export default () => {
  const currentYear = new Intl.DateTimeFormat('fa-IR', {
    year: 'numeric'
  }).format(new Date())

  return (
    <footer className={'footer text-center pt-4 user-select-none'}>
      <span className={'fs-8 text-gray-600'}>
        {`توسعه‌یافته توسط مرکز تحقیق و توسعه همراه اول © ${currentYear}`}
      </span>
    </footer>
  )
}
