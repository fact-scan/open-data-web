import {
  faExchangeAlt,
  faSignOutAlt,
  faUserCircle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Button,
  Col,
  Container,
  Form,
  Image,
  Modal,
  Nav,
  Navbar,
  NavDropdown,
  Row
} from '@themesberg/react-bootstrap'
import Profile3 from 'assets/images/avatar.jpg'
import useHttp from 'hooks/useHttp'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import {
  GetUserAsync,
  resetUser,
  selectIsAdmin,
  selectUser,
  setIsAdmin
} from 'store/slices/UserSlice'

const DEFAULTS = {
  form: {
    firstName: '',
    lastName: '',
    emailaddress: '',
    mobileNumber: ''
  }
}

export default props => {
  const { title } = props
  const dispatch = useDispatch()
  const { push } = useHistory()
  const [isModal, setIsModal] = useState(false)
  const [form, setForm] = useState(DEFAULTS.form)
  const isAdmin = useSelector(selectIsAdmin)
  const user = useSelector(selectUser)
  const { firstName, lastName, mobileNumber, emailaddress } = user
  const [loaddingUpdate, , , onRequestUpdate] = useHttp()

  useEffect(() => {
    if (
      !isModal &&
      (!firstName || !lastName || firstName === 'NULL' || lastName === 'NULL')
    ) {
      setForm({ ...form, firstName, lastName, mobileNumber, emailaddress })
      setIsModal(true)
    }
  }, [form, firstName, lastName, emailaddress, mobileNumber, isModal])

  const onLogOutUser = () => {
    dispatch(resetUser())
  }

  const onEditProfile = () => {
    setForm({ ...form, firstName, lastName, mobileNumber, emailaddress })
    setIsModal(true)
  }

  const onToggleDashboard = () => {
    dispatch(setIsAdmin(!isAdmin))
    push('/')
  }

  const onCloseModal = () => setIsModal(false)

  const onRenderUser = (
    <div className={'media py-0 d-flex align-items-center gap-2'}>
      <Image
        src={Profile3}
        className={'user-avatar md-avatar rounded-circle'}
      />
      <div className={'media-body text-center text-truncate fmw-100 fmxw-150'}>
        {firstName || ''} {lastName || ''}
      </div>
    </div>
  )

  const onGenerateModal = useMemo(() => {
    const onHandleFormChange = event => {
      const { name, value } = event.target
      const newForm = { ...form, [name]: value }
      setForm(newForm)
    }

    const onUpdateUser = async () => {
      const response = await onRequestUpdate('UpdateUserInfoMe', form)

      if (response?.isSuccess) {
        dispatch(GetUserAsync())
        onCloseModal()
      }
    }

    return (
      <Modal show={isModal} onHide={onCloseModal}>
        <Modal.Header
          closeButton={
            firstName && lastName && firstName !== 'NULL' && lastName !== 'NULL'
          }
        >
          <Modal.Title>ویرایش حساب کاربری</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row className={'mb-3'}>
              <Form.Group as={Col}>
                <Form.Label>نام</Form.Label>
                <Form.Control
                  type={'text'}
                  name={'firstName'}
                  value={form.firstName}
                  onChange={onHandleFormChange}
                  placeholder={'لطفا نام خود را وارد کنید'}
                  disabled={loaddingUpdate}
                />
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>نام خانوادگی</Form.Label>
                <Form.Control
                  type={'text'}
                  name={'lastName'}
                  value={form.lastName}
                  onChange={onHandleFormChange}
                  placeholder={'لطفا نام خانوادگی خود را وارد کنید'}
                  disabled={loaddingUpdate}
                />
              </Form.Group>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {firstName &&
            lastName &&
            firstName !== 'NULL' &&
            lastName !== 'NULL' && (
              <Button variant={'danger'} onClick={onCloseModal}>
                بستن
              </Button>
            )}

          <Button
            variant={'success'}
            onClick={onUpdateUser}
            disabled={
              form.firstName.length === 0 ||
              form.lastName.length === 0 ||
              loaddingUpdate
            }
          >
            ویرایش حساب
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }, [
    form,
    firstName,
    lastName,
    loaddingUpdate,
    isModal,
    onRequestUpdate,
    dispatch
  ])

  return (
    <>
      <Navbar
        bg={'soft-primary'}
        className={'text-white py-1 user-select-none'}
      >
        <Container fluid>
          <h2 className={'fs-4 fw-bold m-0 fh-50 d-flex align-items-center'}>
            {title}
          </h2>
          <Nav className={'fh-50 d-flex align-items-center'}>
            <NavDropdown title={onRenderUser} as={Nav.Item} bsPrefix={'py-0'}>
              <NavDropdown.Item
                onClick={onToggleDashboard}
                className={''}
                as={Nav.Link}
              >
                <FontAwesomeIcon
                  icon={faExchangeAlt}
                  size={'lg'}
                  pull={'right'}
                />
                {`داشبورد ${isAdmin ? 'کاربری' : 'مدیریت'}`}
              </NavDropdown.Item>
              <NavDropdown.Item
                onClick={onEditProfile}
                className={''}
                as={Nav.Link}
              >
                <FontAwesomeIcon
                  icon={faUserCircle}
                  size={'lg'}
                  pull={'right'}
                />
                ویرایش حساب کاربری
              </NavDropdown.Item>

              <NavDropdown.Divider />
              <NavDropdown.Item
                onClick={onLogOutUser}
                className={'text-danger'}
                as={Nav.Link}
              >
                <FontAwesomeIcon
                  icon={faSignOutAlt}
                  size={'lg'}
                  pull={'right'}
                />
                خروج از حساب کاربری
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Container>
      </Navbar>
      {onGenerateModal}
    </>
  )
}
