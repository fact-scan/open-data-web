import React from 'react'

export default props => {
  const { show, relative = false, children } = props

  return (
    <>
      <div
        className={`preloader bg-soft flex-column justify-content-center align-items-center ${
          show ? '' : 'show'
        } ${relative ? 'relative' : 'fixed'}`}
      >
        <span className={'loader-element'} />
      </div>
      {children}
    </>
  )
}
