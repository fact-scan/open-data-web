import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Badge, Image, Nav } from '@themesberg/react-bootstrap'
import Logo from 'assets/images/logo-dark.png'
import { Routes } from 'constants/routes'
import React, { useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { CSSTransition } from 'react-transition-group'
import SimpleBar from 'simplebar-react'

export default props => {
  const { isAdmin } = props
  const location = useLocation()
  const { pathname } = location
  const [show, setShow] = useState(false)
  const showClass = show ? 'show' : ''

  const NavItem = props => {
    const {
      title,
      link,
      external,
      target,
      icon,
      image,
      badgeText,
      badgeBg = 'secondary',
      badgeColor = 'primary'
    } = props

    const classNames = badgeText
      ? 'd-flex justify-content-start align-items-center justify-content-between'
      : ''
    const navItemClassName = link === pathname ? 'active' : ''
    const linkProps = external ? { href: link } : { as: Link, to: link }

    return (
      <Nav.Item className={navItemClassName} onClick={() => setShow(false)}>
        <Nav.Link {...linkProps} target={target} className={classNames}>
          <span>
            {icon && (
              <span className={'sidebar-icon'}>
                <FontAwesomeIcon icon={icon} />
              </span>
            )}
            {image && (
              <Image
                src={image}
                width={20}
                height={20}
                className={'sidebar-icon svg-icon'}
              />
            )}

            <span className={'sidebar-text'}>{title}</span>
          </span>
          {badgeText && (
            <Badge
              pill
              bg={badgeBg}
              text={badgeColor}
              className="badge-md notification-count ms-2"
            >
              {badgeText}
            </Badge>
          )}
        </Nav.Link>
      </Nav.Item>
    )
  }

  const onGenerateItems = () => {
    const items = Object.values(isAdmin ? Routes.Admin : Routes.User).filter(
      item => item.sidebar
    )

    return items.map(({ title, sidetitle, path, icon }, index) => (
      <NavItem
        key={`sidebar-item-${index}`}
        title={sidetitle || title}
        link={path}
        icon={icon}
      />
    ))
  }

  return (
    <CSSTransition timeout={300} in={show} classNames="sidebar-transition">
      <SimpleBar
        className={`collapse ${showClass} sidebar d-md-block bg-primary text-white`}
      >
        <div className="sidebar-inner px-2 py-4">
          <Nav className="flex-column pt-3 pt-md-0">
            <div className="d-flex align-items-center justify-content-center pb-4 gap-2">
              <Image src={Logo} className={'mb-2'} />
            </div>
            {onGenerateItems()}
          </Nav>
        </div>
      </SimpleBar>
    </CSSTransition>
  )
}
