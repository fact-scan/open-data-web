import { Col, Container, Form, Row, Spinner } from '@themesberg/react-bootstrap'
import React, { useEffect, useState } from 'react'
import { Cell, Column, HeaderCell, Table } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'
import 'scss/table.scss'

export default props => {
  const {
    items,
    columns,
    loading = true,
    actions,
    bulk = false,
    checkedItems = [],
    isAdmin = false,
    onChecked,
    searchKey,
    helpText = <div className={'fs-8 lh-lg p-1'}>&nbsp;</div>,
    children
  } = props

  const [isLoading, setIsLoading] = useState(loading)
  const [searched, setSearched] = useState([])

  const onChangeHandle = event => {
    setIsLoading(true)
    const search = event.target.value
    setSearched(items.filter(item => item[searchKey].includes(search)))
    setTimeout(() => {
      setIsLoading(false)
    }, 500)
  }

  const onHandleCheck = event => {
    const { value, checked } = event.target
    const oldSelected = bulk ? [...checkedItems, value] : [value]
    const newSelected = checked
      ? oldSelected
      : checkedItems.filter(item => item !== value)
    onChecked(newSelected)
  }

  useEffect(() => {
    setSearched(items)
  }, [items])

  useEffect(() => {
    setIsLoading(loading)
  }, [loading])

  const Custom = {
    Cell: ({ rowData, dataKey, formatter, ...props }) => (
      <Cell {...props}>
        {formatter ? formatter(rowData[dataKey]) : rowData[dataKey]}
      </Cell>
    ),
    Check: ({ rowData, dataKey, onChange, ...props }) => {
      const rowValue = []
      JSON.parse(dataKey).forEach(item => {
        rowValue.push(JSON.stringify(rowData[item]))
      })

      return (
        <Cell {...props} className={'cell-checkbox'}>
          <Form.Check
            type={'checkbox'}
            name={`row_${rowValue.join('|')}`}
            onChange={onHandleCheck}
            value={rowValue.join('|')}
            checked={checkedItems.includes(rowValue.join('|'))}
          />
        </Cell>
      )
    }
  }

  const onHandleScrollbar = event => {
    const events = {
      bodyHeightChanged: { y: 0 },
      bodyWidthChanged: { x: 9999 },
      widthChanged: { x: 9999 },
      heightChanged: { y: 0 }
    }

    return events[event]
  }

  return (
    <Container fluid className={'p-0 mt-4'}>
      <Row className={'mb-0'}>
        <Col xs={{ span: 3 }}>
          {searchKey && (
            <>
              <Form.Control
                type={'search'}
                placeholder={'جستجو...'}
                onChange={onChangeHandle}
              />
              {helpText}
            </>
          )}
        </Col>
        <Col>{actions}</Col>
      </Row>
      <Row>
        <Col>
          <Table
            data={searched}
            rtl={true}
            bordered={true}
            hover={true}
            cellBordered={true}
            virtualized={true}
            renderLoading={() => (
              <div
                className={
                  'position-absolute top-0 start-0 bottom-0 end-0 bg-white'
                }
              >
                <div className={'rs-table-body-info'}>
                  <Spinner animation={'border'} variant={'primary'} />
                </div>
              </div>
            )}
            renderEmpty={() => (
              <div
                className={
                  'position-absolute top-0 start-0 bottom-0 end-0 bg-white'
                }
              >
                <div className={'rs-table-body-info fs-6 text-muted'}>
                  داده‌ای یافت نشد
                </div>
              </div>
            )}
            headerHeight={48}
            autoHeight={true}
            // height={200}
            rowHeight={() => 42}
            loading={isLoading}
            affixHorizontalScrollbar={true}
            // onRowClick={onHandleRowClick}
            // affixHeader
            wordWrap={'break-word'}
            shouldUpdateScroll={onHandleScrollbar}
          >
            {columns
              ? columns.map(
                  (
                    {
                      key,
                      value,
                      header,
                      cell,
                      type,
                      admin,
                      formatter,
                      emit,
                      ...column
                    },
                    index
                  ) =>
                    (isAdmin && admin) || !admin ? (
                      <Column {...column} key={`col_${key}_${index}`}>
                        <HeaderCell {...header}>{value}</HeaderCell>
                        {type === 'checkbox' ? (
                          <Custom.Check
                            dataKey={JSON.stringify(
                              columns
                                .filter(item => item.emit)
                                .map(item => item.key)
                            )}
                            {...cell}
                          />
                        ) : (
                          <Custom.Cell
                            dataKey={key}
                            formatter={formatter}
                            {...cell}
                          />
                        )}
                      </Column>
                    ) : null
                )
              : children}
          </Table>
        </Col>
      </Row>
    </Container>
  )
}
