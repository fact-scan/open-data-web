export const PermissionRequestGoal = {
  Commerical: 0,
  Research: 1,
  Competition: 2,
  InMCI: 3
}

export const FeatureType = {
  Cat: 0,
  Num: 1,
  Ord: 2,
  Txt: 3
}

export const SupportRequestCategory = {
  QuestionSuggestion: 0,
  RaiseDataIssue: 1,
  RaiseSystemIssue: 2,
  other: 3
}

export const StateTypeId = {
  Create: 0,
  Processed: 1,
  Publish: 2,
  Open: 3,
  Remove: 4
}

export const RequestStatus = {
  New: 0,
  Submitted: 1,
  Checked: 2,
  Responsed: 3,
  Accepted: 4,
  Rejected: 5,
  Closed: 6
}
