import Endpoints from 'service/api'
import { InitialConfig } from 'service/defaults'

const pageSize = 100
const pageIndex = 0
const isDelete = false
const orderDesc = true
export const defaultFilter = {
  pageSize,
  pageIndex,
  isDelete,
  orderDesc
}
export const downloadLink = `${InitialConfig.baseURL}/${Endpoints.getFile.url}`
