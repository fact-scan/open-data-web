import {
  faChartPie,
  faDatabase,
  faFileAlt,
  faHandsHelping,
  faList,
  faUserFriends,
  faUsersCog
} from '@fortawesome/free-solid-svg-icons'

export const Routes = {
  Login: {
    path: '/login/',
    page: 'Login',
    private: false,
    title: 'ورود به پنل',
    admin: false,
    sidebar: false,
    alias: false,
    exact: true,
    icon: null
  },

  User: {
    Dashboard: {
      path: '/',
      page: 'Dashboard',
      private: true,
      title: 'داشبورد',
      admin: false,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faChartPie
    },
    Dataset: {
      path: '/dataset/',
      page: 'Dataset',
      private: true,
      title: 'مجموعه دادگان منتشر شده توسط همراه اول',
      sidetitle: 'دادگان',
      admin: false,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faDatabase
    },
    Data: {
      path: '/data/',
      page: 'Data',
      private: true,
      title: 'داده‌های من',
      admin: false,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faFileAlt
    },
    Request: {
      path: '/request/',
      page: 'Request',
      private: true,
      title: 'درخواست‌های من',
      admin: false,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faList
    },
    Support: {
      path: '/support/',
      page: 'Support',
      private: true,
      title: 'پشتیبانی',
      admin: false,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faHandsHelping
    }
  },

  Admin: {
    Dashboard: {
      path: '/',
      page: 'Dashboard',
      private: true,
      title: 'داشبورد',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faChartPie
    },
    Dataset: {
      path: '/dataset/',
      page: 'Dataset',
      private: true,
      title: 'مدیریت دادگان',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faDatabase
    },
    DatasetUser: {
      path: '/dataset/:duid([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/user/',
      redirect: duid => `/dataset/${duid}/user/`,
      page: 'User',
      private: true,
      title: 'کاربران مجاز',
      admin: true,
      sidebar: false,
      alias: true,
      exact: true,
      icon: null
    },
    Process: {
      path: '/dataset/:duid([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/process/:tuid([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/',
      redirect: (duid, tuid) => `/dataset/${duid}/process/${tuid}/`,
      page: 'Process',
      private: true,
      title: 'عملیات دادگان',
      admin: true,
      sidebar: false,
      alias: false,
      exact: true,
      icon: null
    },
    Request: {
      path: '/request/',
      page: 'Request',
      private: true,
      title: 'مدیریت درخواست‌ها',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faList
    },
    User: {
      path: '/user/',
      page: 'User',
      private: true,
      title: 'مدیریت کاربران',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faUserFriends
    },
    UserData: {
      path: '/user/:uuid([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/data/',
      redirect: uuid => `/user/${uuid}/data/`,
      page: 'Data',
      private: true,
      title: 'دسترسی‌های کاربر',
      admin: true,
      sidebar: false,
      alias: true,
      exact: true,
      icon: null
    },
    UserRequest: {
      path: '/user/:uuid([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/request/',
      redirect: uuid => `/user/${uuid}/request/`,
      page: 'Request',
      private: true,
      title: 'درخواست‌های کاربر',
      admin: true,
      sidebar: false,
      alias: true,
      exact: true,
      icon: null
    },
    Role: {
      path: '/role/',
      page: 'Role',
      private: true,
      title: 'مدیریت نقش‌ها',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faUsersCog
    },
    Support: {
      path: '/support/',
      page: 'Support',
      private: true,
      title: 'مدیریت پشتیبانی',
      admin: true,
      sidebar: true,
      alias: false,
      exact: true,
      icon: faHandsHelping
    }
  }
}
