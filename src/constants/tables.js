import { faCheck, faDownload, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { toPersian } from 'src/helper/formatter'
import { downloadLink } from './defaults'

const SUPPORT_MAP = {
  0: 'پرسش و پیشنهاد',
  1: 'گزارش ایراد داده',
  2: 'گزارش ایراد سیستم',
  3: 'سایر'
}

const FORMATTERS = {
  SUPPORT: value => SUPPORT_MAP[value] || '-',
  DATASET: value => value?.shownName || '-',
  TYPE: value => value?.title || '-',
  DATE: value => {
    const date = new Date(value || '2022-09-10T14:18:16.1234325')
    return !value
      ? '-'
      : new Intl.DateTimeFormat('fa-IR', {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        }).format(date)
  },
  DOWNLOAD: value => (
    <FontAwesomeIcon
      icon={faDownload}
      role={'button'}
      size={'lg'}
      onClick={() => window.open(`${downloadLink}/${value}`, '_blank')}
    />
  ),
  STATUS: (value, status) => (
    <FontAwesomeIcon
      icon={value >= status ? faCheck : faTimes}
      color={value >= status ? '#05a677' : '#fa5252'}
      size={'lg'}
    />
  ),
  STATUS_REQUEST: value => {
    const ENUMS = {
      0: { title: 'ثبت شده', class: 'text-purple' },
      1: { title: 'ثبت شده', class: 'text-purple' },
      2: { title: 'درحال بررسی', class: 'text-info' },
      3: { title: 'پاسخ داده شده', class: 'text-info' },
      4: { title: 'پذیرفته شده', class: 'text-success' },
      5: { title: 'رد شده', class: 'text-danger' },
      6: { title: 'بسته شده', class: 'text-soft-primary' }
    }
    return (
      <span className={`${ENUMS[value]?.class || ''} fw-medium`}>
        {ENUMS[value]?.title || '-'}
      </span>
    )
  },
  STATUS_SUPPORT: value => {
    const ENUMS = {
      0: { title: 'ثبت شده', class: 'text-purple' },
      1: { title: 'ثبت شده', class: 'text-purple' },
      2: { title: 'درحال بررسی', class: 'text-info' },
      3: { title: 'پاسخ داده شده', class: 'text-info' },
      4: { title: 'پذیرفته شده', class: 'text-success' },
      5: { title: 'رد شده', class: 'text-danger' },
      6: { title: 'بسته شده', class: 'text-soft-primary' }
    }
    return (
      <span className={`${ENUMS[value]?.class || ''} fw-medium`}>
        {ENUMS[value]?.title || '-'}
      </span>
    )
  },
  NUMBER: value => toPersian(value || 0),
  ROLE: value =>
    value.length ? value.map(role => role.title).join(' - ') : value || '-',
  SIZE: value => (
    <>
      {toPersian(value || 0)}
      <span className={'fs-8 ms-2 me-0'}>KB</span>
    </>
  ),
  MOBILE: value => toPersian(value?.mobileNumber || ''),
  USERNAME: value => `${value?.firstName || ''} ${value?.lastName || ''}`
}

const COLUMNS = {
  type: 'normal',
  header: {},
  cell: {},
  align: 'center',
  verticalAlign: 'middle',
  admin: false,
  emit: false
}

export const DATASET = {
  COLUMNS: [
    {
      ...COLUMNS,
      key: 'datasetId',
      emit: true,
      value: '',
      type: 'checkbox',
      width: 48,
      fixed: true
    },
    {
      ...COLUMNS,
      key: 'shownName',
      value: 'نام مجموعه دادگان',
      minWidth: 200,
      flexGrow: 4
    },
    {
      ...COLUMNS,
      key: 'datasetType',
      emit: true,
      value: 'نوع مجموعه دادگان',
      minWidth: 250,
      flexGrow: 5,
      formatter: FORMATTERS.TYPE
    },
    {
      ...COLUMNS,
      key: 'publishedDate',
      value: 'تاریخ انتشار',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      key: 'expireDate',
      value: 'تاریخ انقضا',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      cell: { dir: 'ltr' },
      key: 'size',
      value: 'حجم فایل',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.SIZE
    },
    {
      ...COLUMNS,
      key: 'recordCount',
      value: 'تعداد رکورد',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.NUMBER
    },
    {
      ...COLUMNS,
      key: 'datasetStateTypeId',
      value: 'پردازش شده',
      minWidth: 100,
      flexGrow: 2,
      admin: true,
      formatter: value => FORMATTERS.STATUS(value, 1)
    },
    {
      ...COLUMNS,
      key: 'datasetStateTypeId',
      value: 'منتشر شده',
      minWidth: 100,
      flexGrow: 2,
      admin: true,
      formatter: value => FORMATTERS.STATUS(value, 2)
    },
    {
      ...COLUMNS,
      key: 'datasetStateTypeId',
      value: 'آزاد شده',
      minWidth: 100,
      flexGrow: 2,
      admin: true,
      formatter: value => FORMATTERS.STATUS(value, 3)
    },
    {
      ...COLUMNS,
      key: 'descFileId',
      value: 'فایل مستندات',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.DOWNLOAD
    }
  ]
}

export const PROCESS = {
  CATEGORY: [
    { id: -1, title: 'نام فیلد' },
    { id: 0, title: 'استانداردسازی' },
    { id: 1, title: 'پاک‌سازی' },
    { id: 2, title: 'نرمال‌سازی' },
    { id: 3, title: 'گمنام‌سازی' }
  ]
}

export const REQUEST = {
  COLUMNS: [
    {
      ...COLUMNS,
      key: 'datasetPermissionRequestId',
      emit: true,
      value: '',
      type: 'checkbox',
      width: 48,
      fixed: true
    },
    {
      ...COLUMNS,
      key: 'dataset',
      value: 'نام مجموعه دادگان',
      minWidth: 200,
      flexGrow: 4,
      formatter: FORMATTERS.DATASET
    },
    {
      ...COLUMNS,
      key: 'user',
      value: 'درخواست‌دهنده',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.USERNAME
    },
    {
      ...COLUMNS,
      key: 'user',
      value: 'شماره تماس',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.MOBILE
    },
    {
      ...COLUMNS,
      key: 'createDate',
      value: 'تاریخ درخواست',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      key: 'state',
      value: 'وضعیت درخواست',
      minWidth: 150,
      flexGrow: 3,
      formatter: value => FORMATTERS.STATUS_REQUEST(value)
    }
  ]
}

export const USER = {
  COLUMNS: [
    {
      ...COLUMNS,
      key: 'userId',
      emit: true,
      value: '',
      type: 'checkbox',
      width: 48,
      fixed: true
    },
    {
      ...COLUMNS,
      key: 'user',
      value: 'نام و نام خانوادگی',
      minWidth: 250,
      flexGrow: 5,
      admin: true,
      formatter: FORMATTERS.USERNAME
    },
    {
      ...COLUMNS,
      key: 'mobileNumber',
      value: 'شماره تماس',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.NUMBER
    },
    {
      ...COLUMNS,
      key: 'roles',
      value: 'نقش کاربر',
      minWidth: 150,
      admin: true,
      flexGrow: 3,
      formatter: FORMATTERS.ROLE
    },
    {
      ...COLUMNS,
      key: 'createDate',
      value: 'تاریخ عضویت',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.DATE
    }
    // {
    //   ...COLUMNS,
    //   key: 'type',
    //   value: 'نوع کاربر',
    //   minWidth: 150,
    //   admin: true,
    //   flexGrow: 3
    // }
  ]
}

export const SUPPORT = {
  COLUMNS: [
    {
      ...COLUMNS,
      key: 'supportRequestId',
      emit: true,
      value: '',
      type: 'checkbox',
      width: 48,
      fixed: true
    },
    {
      ...COLUMNS,
      key: 'categoryId',
      value: 'دسته‌بندی درخواست',
      minWidth: 200,
      flexGrow: 4,
      formatter: FORMATTERS.SUPPORT
    },
    {
      ...COLUMNS,
      key: 'dataset',
      value: 'نام مجموعه دادگان',
      minWidth: 200,
      flexGrow: 4,
      formatter: FORMATTERS.DATASET
    },
    {
      ...COLUMNS,
      key: 'creator',
      value: 'درخواست‌دهنده',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.USERNAME
    },
    {
      ...COLUMNS,
      key: 'creator',
      value: 'شماره تماس',
      minWidth: 150,
      flexGrow: 3,
      admin: true,
      formatter: FORMATTERS.MOBILE
    },
    {
      ...COLUMNS,
      key: 'createDate',
      value: 'تاریخ درخواست',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      key: 'stateId',
      value: 'وضعیت درخواست',
      minWidth: 150,
      flexGrow: 3,
      formatter: value => FORMATTERS.STATUS_SUPPORT(value)
    }
  ]
}

export const DATA = {
  COLUMNS: [
    {
      ...COLUMNS,
      key: 'datasetId',
      emit: true,
      value: '',
      type: 'checkbox',
      width: 48,
      fixed: true
    },
    {
      ...COLUMNS,
      key: 'shownName',
      value: 'نام مجموعه دادگان',
      minWidth: 200,
      flexGrow: 4
    },
    {
      ...COLUMNS,
      key: 'datasetType',
      value: 'نوع مجموعه دادگان',
      minWidth: 250,
      flexGrow: 5,
      formatter: FORMATTERS.TYPE
    },
    {
      ...COLUMNS,
      key: 'publishedDate',
      value: 'تاریخ انتشار',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      key: 'expireDate',
      value: 'تاریخ انقضا',
      minWidth: 150,
      flexGrow: 3,
      formatter: FORMATTERS.DATE
    },
    {
      ...COLUMNS,
      cell: { dir: 'ltr' },
      key: 'size',
      value: 'حجم فایل',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.SIZE
    },
    {
      ...COLUMNS,
      key: 'recordCount',
      value: 'تعداد رکورد',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.NUMBER
    },
    {
      ...COLUMNS,
      key: 'descFileId',
      emit: true,
      value: 'فایل مستندات',
      minWidth: 100,
      flexGrow: 2,
      formatter: FORMATTERS.DOWNLOAD
    }
  ]
}
