import { useCallback, useEffect, useState } from 'react'

const useCountDown = (initial = { d: 0, h: 0, m: 0, s: 0 }) => {
  const INITIAL = {
    d: initial.d || 0,
    h: initial.h || 0,
    m: initial.m || 0,
    s: initial.s || 0
  }
  const [end, setEnd] = useState(0)
  const [stop, setStop] = useState(true)
  const [counter, setCounter] = useState(INITIAL)

  const onCalculateCountDown = useCallback(() => {
    const difference = end - +new Date()
    if (difference > 0) {
      setCounter({
        d: Math.floor(difference / (1000 * 60 * 60 * 24)),
        h: Math.floor((difference / (1000 * 60 * 60)) % 24),
        m: Math.floor((difference / 1000 / 60) % 60),
        s: Math.floor((difference / 1000) % 60)
      })
    }
  }, [end])

  const onCalculateMiliSecond = countDown => {
    return (
      countDown.s * 1000 +
      countDown.m * 60 * 1000 +
      countDown.h * 60 * 60 * 1000 +
      countDown.d * 24 * 60 * 60 * 1000
    )
  }

  const onReset = () => {
    setCounter(INITIAL)
    if (end > 0) {
      setTimeout(() => {
        setEnd(+new Date() + +new Date(onCalculateMiliSecond(INITIAL)))
      }, 1000)
    }
  }

  const onStart = () => {
    if (end === 0) {
      setStop(false)
      setTimeout(() => {
        setEnd(+new Date() + +new Date(onCalculateMiliSecond(counter)))
      }, 1000)
    }
  }

  const onStop = () => {
    if (end > 0) {
      setStop(true)
      setEnd(0)
    }
  }

  useEffect(() => {
    onCalculateCountDown()
  }, [onCalculateCountDown, end])

  useEffect(() => {
    const handler = setTimeout(() => {
      onCalculateCountDown()
    }, 1000)

    return () => {
      clearTimeout(handler)
    }
  }, [onCalculateCountDown, counter])

  const onPaddingCounter = countDown => {
    const _countDown = {}
    for (const [key, value] of Object.entries(countDown)) {
      if (Object.prototype.hasOwnProperty.call(countDown, key)) {
        _countDown[key] = value.toString().padStart(2, '0')
      }
    }
    return _countDown
  }

  const onStringCounter = countDown => {
    const _countDown = onPaddingCounter(countDown)
    return Object.keys(_countDown)
      .filter(_item => _item !== 'd' && _item !== 'h')
      .map(_item => _countDown[_item])
      .join(':')
  }

  const isEnded = () => {
    return (
      counter.d === 0 && counter.h === 0 && counter.m === 0 && counter.s === 0
    )
  }

  const isStopped = () => {
    return !isEnded() && stop
  }

  return [
    counter,
    onPaddingCounter(counter),
    onStringCounter(counter),
    isEnded(),
    isStopped(),
    onStart,
    onStop,
    onReset
  ]
}

export default useCountDown
