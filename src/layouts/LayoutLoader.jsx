import Preloader from 'components/Preloader'
import React, { useEffect, useState } from 'react'
import { Route } from 'react-router-dom'

const LayoutLoader = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000)
    return () => clearTimeout(timer)
  }, [])

  return (
    <Route
      {...rest}
      render={props => (
        <Preloader show={loaded ? false : true}>
          <Component {...props} />
        </Preloader>
      )}
    />
  )
}

export default LayoutLoader
