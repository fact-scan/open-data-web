import Preloader from 'components/Preloader'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

const LayoutNotFound = () => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000)
    return () => clearTimeout(timer)
  }, [])

  return (
    <Preloader show={loaded ? false : true}>
      <div className="d-flex align-items-center justify-content-center vh-100 bg-primary text-white">
        <div className="text-center">
          <h1 className="display-1 lh-1 text-danger">404</h1>
          <p className="display-4 text-danger">یافت نشد!</p>
          <p className="lead">صفحه‌ای که در جستجوی آن هستید وجود ندارد.</p>
          <Link to={'/'} className="btn btn-light">
            بازگشت به صفحه اصلی
          </Link>
        </div>
      </div>
    </Preloader>
  )
}

export default LayoutNotFound
