import Footer from 'components/Footer'
import Navbar from 'components/Navbar'
import Preloader from 'components/Preloader'
import Sidebar from 'components/Sidebar'
import React, { useEffect, useState } from 'react'
import { Route } from 'react-router-dom'
import SimpleBar from 'simplebar-react'

const LayoutSidebar = ({ component: Component, title, admin, ...rest }) => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000)
    return () => clearTimeout(timer)
  }, [])

  return (
    <Route
      {...rest}
      render={props => (
        <Preloader show={loaded ? false : true}>
          <Sidebar isAdmin={admin} />

          <main className={'content'}>
            <Navbar title={title} />
            <SimpleBar className={'main-content'} autoHide={false}>
              <div className={'wrapper'}>
                <div className="d-flex flex-column align-items-stretch flex-grow-1">
                  <Component {...props} />
                </div>
                <Footer />
              </div>
            </SimpleBar>
          </main>
        </Preloader>
      )}
    />
  )
}

export default LayoutSidebar
