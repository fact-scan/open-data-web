import React from 'react'

export default props => {
  const { isAdmin } = props
  return <div>{isAdmin ? 'داشبورد مدیریت' : 'داشبورد کاربری'}</div>
}
