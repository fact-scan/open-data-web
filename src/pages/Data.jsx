import { faDownload } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Form, Modal } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { defaultFilter, downloadLink } from 'constants/defaults'
import { DATA } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import React, { useCallback, useEffect, useState } from 'react'

const DEFAULTS = {
  form: {
    datasetPermissionRequestId: null,
    response: '',
    state: 0
  }
}

export default props => {
  const { isAdmin, isAlias } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)
  const [items, setItems] = useState([])
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [formData, setFormData] = useState(null)
  const [loadingItems, responseItems, errorItems, onRequestItems] = useHttp()
  const [loadingState, , , onRequestState] = useHttp()
  const [loadingData, , , onRequestData] = useHttp()

  const onGetItems = useCallback(() => {
    onRequestItems(
      isAdmin
        ? 'GetAllRellUserDatasetPermissionForAdmin'
        : 'GetAllRellUserDatasetPermissionForMe',
      filter
    )
  }, [onRequestItems, filter, isAdmin])

  const onChangeRequestState = async () => {
    const response = await onRequestState('ChangePermissionRequestState', {
      datasetPermissionRequestId: items.find(item =>
        selected
          .map(sitem => JSON.parse(sitem.split('|')[0]))
          .includes(item.datasetPermissionRequestId)
      )?.datasetPermissionRequestId,
      response: form.response || '',
      state:
        isModal === 'confirm'
          ? 4
          : isModal === 'reject' || isModal !== 'check'
          ? 5
          : form.state
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  useEffect(() => {
    onGetItems()
  }, [onGetItems])

  useEffect(() => {
    if (responseItems?.isSuccess) {
      setItems([...responseItems?.value?.dataList.map(item => item?.dataset)])
    }
    if (errorItems?.isFailed) {
      console.warn(
        `Error: ${
          isAdmin
            ? 'GetAllRellUserDatasetPermissionForAdmin'
            : 'GetAllRellUserDatasetPermissionForMe'
        }`,
        errorItems
      )
    }
  }, [responseItems, isAdmin, errorItems])

  useEffect(() => {
    if (isModal === 'check' && formData === null) {
      ;(async () => {
        const response = await onRequestData('GetPermissionRequest', {
          datasetPermissionRequestId: items.find(item =>
            selected
              .map(sitem => JSON.parse(sitem.split('|')[0]))
              .includes(item.datasetPermissionRequestId)
          )?.datasetPermissionRequestId
        })
        if (response?.value) {
          setFormData(response?.value)
          setForm({
            ...form,
            response: response?.response || '',
            state: parseInt(response?.value?.state || 0)
          })
        }
      })()
    }
    if (!isModal) {
      setForm(DEFAULTS.form)
      setFormData(null)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModal])

  const onHandleFormChange = event => {
    const { name, value, checked } = event.target
    const newForm = {
      normal: { ...form, [name]: value },
      state: {
        ...form,
        [name]: checked ? parseInt(value) : formData?.state || 0
      }
    }
    setForm(newForm[name] || newForm['normal'])
  }

  const onHandleSelected = checkedItems => {
    setSelected(checkedItems)
  }

  const onDownloadFile = () => {
    const ids = selected[0].split('|')
    const duid = JSON.parse(ids[0])
    const fileId = items.find(item => item.datasetId === duid)?.fileId
    window.open(`${downloadLink}/${fileId}`, '_blank')
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateTitle = {
    check: 'بررسی دسترسی'
  }

  const onGenerateBody = {
    check: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>هدف از درخواست</Form.Label>
          <Form.Select
            name={'goalCat'}
            className={'mb-1'}
            value={formData?.goalCat || ''}
            disabled={true}
          >
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={0}>استفاده تجاری</option>
            <option value={1}>استفاده تحقیقاتی</option>
            <option value={2}>مسابقه</option>
            <option value={3}>درون سازمانی</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <div className={'fs-6 text-gray-600 fmh-50'}>
            {formData?.description || ''}
          </div>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Button
            onClick={() =>
              window.open(`${downloadLink}/${formData?.fileId}`, '_blank')
            }
            variant={'soft-primary'}
            disabled={!formData?.fileId}
          >
            <FontAwesomeIcon icon={faDownload} pull={'right'} size={'lg'} />
            <span>
              {formData?.fileId
                ? 'دانلود اسناد بارگذاری شده'
                : 'کاربر اسنادی بارگذاری نکرده است'}
            </span>
          </Button>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>پاسخ به درخواست</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={3}
            name={'response'}
            value={form.response}
            onChange={onHandleFormChange}
            placeholder={'لطفا پاسخ درخواست را وارد کنید'}
            disabled={loadingState || loadingData}
          />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <div
            className={'d-flex align-items-center justify-content-start gap-2'}
          >
            <Form.Check
              type={'checkbox'}
              id={'state'}
              name={'state'}
              value={4}
              onChange={onHandleFormChange}
              checked={form.state === 4}
              disabled={loadingState || loadingData}
            />
            <Form.Label className={'m-0'} htmlFor={'state'} role={'button'}>
              تأیید دسترسی پس از ارسال
            </Form.Label>
          </div>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <div
            className={'d-flex align-items-center justify-content-start gap-2'}
          >
            <Form.Check
              type={'checkbox'}
              id={'state'}
              name={'state'}
              value={5}
              onChange={onHandleFormChange}
              checked={form.state === 5}
              disabled={loadingState || loadingData}
            />
            <Form.Label className={'m-0'} htmlFor={'state'} role={'button'}>
              رد دسترسی پس از ارسال
            </Form.Label>
          </div>
        </Form.Group>
      </Form>
    )
  }

  const onGenerateFooter = {
    check: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onChangeRequestState}
          disabled={loadingState || loadingData}
        >
          ثبت بررسی درخواست
        </Button>
      </>
    )
  }

  const onGenerateActions = (
    <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
      {isAdmin && isAlias ? (
        <>
          {selected.length > 0 && (
            <Button variant={'success'} onClick={() => onShowModal('check')}>
              اضافه کردن دسترسی
            </Button>
          )}
        </>
      ) : (
        <>
          {selected.length > 0 && (
            <Button variant="success" onClick={onDownloadFile}>
              دانلود
            </Button>
          )}
        </>
      )}
      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
        <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
      </Modal>
    </div>
  )

  return (
    <Table
      items={items}
      loading={loadingItems}
      columns={DATA.COLUMNS}
      checkedItems={selected}
      onChecked={onHandleSelected}
      isAdmin={isAdmin}
      actions={onGenerateActions}
      helpText={
        <div className={'fs-8 lh-lg p-1'}>
          برای دانلود رکورد مورد نظر را انتخاب کنید
        </div>
      }
      searchKey={'shownName'}
    />
  )
}
