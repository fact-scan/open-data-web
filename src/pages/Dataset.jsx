import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Button,
  Col,
  Form,
  Modal,
  Row,
  Spinner
} from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { defaultFilter } from 'constants/defaults'
import { Routes } from 'constants/routes'
import { DATASET } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import { jalaaliToDateObject } from 'jalaali-js'
import React, { useCallback, useEffect, useState } from 'react'
import { Calendar, utils } from 'react-modern-calendar-datepicker'
import 'react-modern-calendar-datepicker/lib/DatePicker.css'
import { useHistory } from 'react-router-dom'
import { toPersian } from 'src/helper/formatter'

const DEFAULTS = {
  form: {
    name: '',
    shownName: '',
    datasetTypeId: '',
    termAndConditionId: 0,
    applicationTypeId: '',
    datasetDasc: '',
    permissionDasc: '',
    fileId: '',
    descFileId: ''
  },
  request: {
    goalCat: '',
    goalText: '',
    description: '',
    dataPermission: 0,
    fileId: '',
    datasetId: '',
    accept: 0,
    permissionDasc: ''
  },
  state: {
    publishedDate: null,
    expireDate: null,
    datasetStateTypeId: 0
  },
  dates: {
    now: true,
    publishedDate: false,
    expireDate: false
  },
  fileFields: {
    fileId: false,
    descFileId: false
  }
}

export default props => {
  const { push } = useHistory()
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)
  const [items, setItems] = useState([])
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [request, setRequest] = useState(DEFAULTS.request)
  const [state, setState] = useState(DEFAULTS.state)
  const [types, setTypes] = useState(null)
  const [isForbidden, setIsForbidden] = useState(false)
  const [fileFields, setFileFields] = useState(DEFAULTS.fileFields)
  const [loadingItems, responseItems, errorItems, onRequestItems] = useHttp()
  const [loaddingAdd, , , onRequestAdd] = useHttp()
  const [loaddingPermission, , , onRequestPermission] = useHttp()
  const [loaddingDelete, , , onRequestDelete] = useHttp()
  const [loaddingType, , , onRequestType] = useHttp()
  const [loadingUpload, , , onRequestUpload] = useHttp()
  const [loadingState, , , onRequestState] = useHttp()
  const [isPicker, setIsPicker] = useState(DEFAULTS.dates)

  const onGetItems = useCallback(() => {
    onRequestItems(isAdmin ? 'GetAllDatasetForAdmin' : 'GetAllDataset', filter)
  }, [onRequestItems, filter, isAdmin])

  const onSubmitDataset = async () => {
    const response = await onRequestAdd('AddDataset', {
      ...form,
      name: form.shownName
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onSubmitRequest = async () => {
    const ids = selected[0].split('|')
    const duid = JSON.parse(ids[0])
    const response = await onRequestPermission('AddPermissionRequest', {
      goalCat: request.goalCat,
      goalText: request.goalText,
      description: request.description,
      dataPermission: request.dataPermission,
      fileId: request.fileId.length > 0 ? request.fileId : null,
      datasetId: duid
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onDeleteDataset = async () => {
    const ids = selected[0].split('|')
    const datasetId = JSON.parse(ids[0])
    const response = await onRequestDelete('DeleteDataset', { datasetId })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onUpdateDatasetState = async () => {
    const nowTime = new Date()
    let publishedDate = state.publishedDate || null
    let expireDate = state.expireDate || null

    if (publishedDate) {
      const { year: py, month: pm, day: pd } = publishedDate
      publishedDate = jalaaliToDateObject(py, pm, pd)
      publishedDate.setHours(nowTime.getHours())
      publishedDate.setMinutes(nowTime.getMinutes())
      publishedDate = publishedDate.toISOString()
    }
    if (expireDate) {
      const { year: ey, month: em, day: ed } = expireDate
      expireDate = jalaaliToDateObject(ey, em, ed)
      expireDate.setHours(nowTime.getHours())
      expireDate.setMinutes(nowTime.getMinutes())
      expireDate = expireDate.toISOString()
    }

    const response = await onRequestState('UpdateDatasetState', {
      ...state,
      publishedDate,
      expireDate
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  useEffect(() => {
    onGetItems()
  }, [onGetItems])

  useEffect(() => {
    if (responseItems?.isSuccess) {
      setItems([...responseItems?.value?.dataList])
    }
    if (errorItems?.isFailed) {
      console.warn(
        `Error: ${isAdmin ? 'GetAllDatasetForAdmin' : 'GetAllDataset'}`,
        errorItems
      )
    }
  }, [responseItems, isAdmin, errorItems])

  useEffect(() => {
    if (isModal === 'add' && types === null) {
      ;(async () => {
        const response = await onRequestType('GetAllDatasetType')
        if (response?.value?.dataList?.length) {
          setTypes(response?.value?.dataList)
        }
      })()
    }
	
	}
    if (isModal === 'request') {
      const ids = selected[0].split('|')
      const duid = JSON.parse(ids[0])
      const permissionDasc =
        items.find(item => item.datasetId === duid)?.permissionDasc || '-'
      setForm({ ...form, permissionDasc })
    }
    if (isModal === 'free' || isModal === 'publish') {
      const ids = selected[0].split('|')
      const duid = JSON.parse(ids[0])
      const state =
        items.find(item => item.datasetId === duid)?.datasetStateTypeId || 0
      if (isModal === 'publish' && state < 1) {
        setIsForbidden(true)
      } else if (isModal === 'free' && state < 2) {
        setIsForbidden(true)
      } else {
        setState({
          ...state,
          datasetId: duid,
          datasetStateTypeId: isModal === 'publish' ? 2 : 3
        })
      }
    }
    if (!isModal) {
      setForm(DEFAULTS.form)
      setRequest(DEFAULTS.request)
      setState(DEFAULTS.state)
      setFileFields(DEFAULTS.fileFields)
      setIsPicker(DEFAULTS.dates)
      setIsForbidden(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModal])

  const onHandleFormChange = event => {
    const { name, value, checked } = event.target
    const newForm =
      isModal === 'request'
        ? {
            normal: { ...request, [name]: value },
            goalCat: { ...request, [name]: parseInt(value) },
            accept: {
              ...request,
              [name]: checked ? parseInt(value) : 0
            }
          }
        : {
            normal: { ...form, [name]: value },
            applicationTypeId: { ...form, [name]: parseInt(value) },
            termAndConditionId: {
              ...form,
              [name]: checked ? parseInt(value) : 0
            }
          }

    isModal === 'request'
      ? setRequest(newForm[name] || newForm['normal'])
      : setForm(newForm[name] || newForm['normal'])
  }

  const onHandleDates = (name, date) => {
    setState({ ...state, [name]: date })
    setIsPicker({ ...isPicker, [name]: false })
  }

  const onHandleFile = event => {
    const { name, files } = event.target
    setFileFields({ ...fileFields, [name]: true })
    const fileType = files[0].type || 'text/csv'
    const fileName = files[0].name || 'file.csv'
    const file = new File([files[0]], fileName, { type: fileType })

    setTimeout(async () => {
      const response = await onRequestUpload('UploadFile', {
        inAppFolder: false,
        file
      })
      if (response?.value?.fileId) {
        isModal === 'request'
          ? setRequest({ ...request, [name]: response.value.fileId })
          : setForm({ ...form, [name]: response.value.fileId })
      } else {
        setFileFields({ ...fileFields, [name]: false })
      }
    }, 1000)
  }

  const onRemoveFile = field => {
    setFileFields({ ...fileFields, [field]: false })
    setForm({ ...form, [field]: '' })
  }

  const onHandleSelected = checkedItems => {
    setSelected(checkedItems)
  }

  const onGoToProcess = () => {
    const ids = selected[0].split('|')
    const duid = JSON.parse(ids[0])
    const tuid = JSON.parse(ids[1])?.datasetTypeId
    push(Routes.Admin.Process.redirect(duid, tuid))
  }

  const onGoToPermissionUsers = () => {
    const ids = selected[0].split('|')
    const duid = JSON.parse(ids[0])
    push(Routes.Admin.DatasetUser.redirect(duid))
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateTitle = {
    request: 'درخواست دسترسی',
    add: 'افزودن دادگان جدید',
    delete: 'حذف مجموعه دادگان؟',
    free: 'آزادسازی مجموعه دادگان',
    publish: 'انتشار مجموعه دادگان'
  }

  const onGenerateStateBody = (stateTypeId = 0) => (
    <Form
      onClick={() => {
        setIsPicker({
          ...isPicker,
          publishedDate: false,
          expireDate: false
        })
      }}
    >
      <div className={'mb-3'}>
        شما در حال {stateTypeId === 2 ? 'انتشار' : 'آزادسازی'}
        <span className={'fw-medium text-success d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetId)
            )
            .map(item => item.shownName)
            .join(' و ')}
        </span>
        هستید.
      </div>

      <Row className={'mb-3'}>
        <Form.Group as={Col}>
          <div
            className={'d-flex align-items-center justify-content-start gap-2'}
          >
            <Form.Check
              type={'checkbox'}
              id={'now'}
              name={'now'}
              onChange={({ target: { checked } }) => {
                setState({
                  ...state,
                  publishedDate: null,
                  expireDate: null
                })
                setIsPicker({
                  publishedDate: false,
                  expireDate: false,
                  now: checked
                })
              }}
              defaultChecked={true}
              disabled={loadingState}
            />
            <Form.Label className={'m-0 fw-medium'} htmlFor={'now'}>
              همین الان {stateTypeId === 2 ? 'منتشر' : 'آزادسازی'} کن
            </Form.Label>
          </div>
        </Form.Group>
      </Row>
      {!isPicker.now && (
        <Row className={'mb-3'}>
          <Form.Group
            as={Col}
            className={'position-relative'}
            onClick={event => event.stopPropagation()}
          >
            <Form.Label>تاریخ انتشار</Form.Label>
            <Form.Control
              type={'text'}
              name={'publishedDate'}
              defaultValue={
                (state.publishedDate &&
                  toPersian(
                    `${state.publishedDate?.year}/${state.publishedDate?.month}/${state.publishedDate?.day}`
                  )) ||
                ''
              }
              onFocus={() => {
                if (!loadingState) {
                  setIsPicker({
                    publishedDate: true,
                    expireDate: false
                  })
                }
              }}
              placeholder={'انتخاب تاریخ انتشار'}
              readOnly={true}
              disabled={loadingState}
            />
            {isPicker.publishedDate && (
              <Calendar
                value={state.publishedDate}
                onChange={date => onHandleDates('publishedDate', date)}
                shouldHighlightWeekends={true}
                minimumDate={utils('fa').getToday()}
                locale={'fa'}
                calendarClassName={'position-absolute top-0 start-0 mt-5 ms-1'}
              />
            )}
          </Form.Group>
          <Form.Group
            as={Col}
            className={'position-relative'}
            onClick={event => event.stopPropagation()}
          >
            <Form.Label>تاریخ انقضا</Form.Label>
            <Form.Control
              type={'text'}
              name={'expireDate'}
              defaultValue={
                (state.expireDate &&
                  toPersian(
                    `${state.expireDate?.year}/${state.expireDate?.month}/${state.expireDate?.day}`
                  )) ||
                ''
              }
              onFocus={() => {
                if (!loadingState) {
                  setIsPicker({
                    publishedDate: false,
                    expireDate: true
                  })
                }
              }}
              placeholder={'انتخاب تاریخ انقضا'}
              readOnly={true}
              disabled={loadingState}
            />
            {isPicker.expireDate && (
              <Calendar
                value={state.expireDate}
                onChange={date => onHandleDates('expireDate', date)}
                shouldHighlightWeekends={true}
                minimumDate={state.publishedDate || utils('fa').getToday()}
                locale={'fa'}
                calendarClassName={'position-absolute top-0 start-0 mt-5 ms-1'}
              />
            )}
          </Form.Group>
        </Row>
      )}
    </Form>
  )

  const onGenerateBody = {
    request: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>هدف از درخواست</Form.Label>
          <Form.Select
            name={'goalCat'}
            className={'mb-1'}
            value={request.goalCat}
            onChange={onHandleFormChange}
            disabled={loaddingPermission}
          >
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={0}>استفاده تجاری</option>
            <option value={1}>استفاده تحقیقاتی</option>
            <option value={2}>مسابقه</option>
            <option value={3}>درون سازمانی</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * با انتخاب صحیح دسته‌بندی درخواست مجموعه دادگان ما را در بهبود
            سامانه یاری کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={3}
            name={'description'}
            value={request.description}
            onChange={onHandleFormChange}
            placeholder={'لطفا شرح درخواست را وارد کنید'}
            disabled={loaddingPermission}
          />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>اسناد مورد نیاز</Form.Label>
          <Form.Text as={'div'} className={'text-info'}>
            {form.permissionDasc}
          </Form.Text>
        </Form.Group>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>بارگذاری فایل مستندات</Form.Label>
            <div
              className={'d-flex align-items-center justify-content-between'}
            >
              <Button
                variant={'primary'}
                onClick={() => document.getElementById('fileId').click()}
                disabled={
                  loaddingPermission ||
                  loadingUpload ||
                  fileFields.fileId ||
                  (fileFields.fileId && request.fileId.length === 0)
                }
              >
                انتخاب فایل
              </Button>
              {fileFields.fileId &&
                (request.fileId.length > 0 ? (
                  <div
                    className={'d-flex align-items-center gap-2 text-danger'}
                    role={'button'}
                    onClick={() => onRemoveFile('fileId')}
                  >
                    <FontAwesomeIcon icon={faTimesCircle} size={'sm'} />
                    <span className={'fs-8'}>حذف فایل</span>
                  </div>
                ) : (
                  <div
                    className={'d-flex align-items-center gap-2 text-success'}
                  >
                    <Spinner animation={'border'} size={'sm'} />
                    <span className={'fs-8'}>در حال بارگذاری</span>
                  </div>
                ))}
            </div>
            <Form.Control
              type={'file'}
              accept={'.xlsx,.xls,.csv,.doc,.docx,.pdf,.txt,.png,.jpg,.jpeg'}
              id={'fileId'}
              name={'fileId'}
              className={'file'}
              onChange={onHandleFile}
              multiple={false}
              hidden={true}
            />
          </Form.Group>

          <Form.Group as={Col} />
        </Row>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <div
              className={
                'd-flex align-items-center justify-content-start gap-2'
              }
            >
              <Form.Check
                type={'checkbox'}
                id={'accept'}
                name={'accept'}
                value={1}
                onChange={onHandleFormChange}
                checked={request.accept === 1}
                disabled={loaddingPermission}
              />
              <Form.Label className={'m-0 fw-medium'} htmlFor={'accept'}>
                با ثبت این درخواست
                <a
                  className={'fw-bold mx-1 text-danger'}
                  href={'https://urls.st/dataagreement'}
                  target={'_blank'}
                  rel="noopener noreferrer"
                >
                  «قوانین و مقررات»
                </a>
                را می‌پذیرم.
              </Form.Label>
            </div>
          </Form.Group>
        </Row>
      </Form>
    ),
    add: (
      <Form>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>نام مجموعه داده</Form.Label>
            <Form.Control
              type={'text'}
              name={'shownName'}
              value={form.shownName}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام مجموعه داده را وارد کنید'}
              disabled={loaddingType || loaddingAdd}
            />
          </Form.Group>
          <Form.Group as={Col} hidden={true}>
            <div
              className={
                'd-flex align-items-center justify-content-center h-100 gap-2'
              }
            >
              <Form.Check
                type={'checkbox'}
                id={'termAndConditionId'}
                name={'termAndConditionId'}
                value={1}
                onChange={onHandleFormChange}
                checked={form.termAndConditionId === 1}
                disabled={loaddingType || loaddingAdd}
              />
              <Form.Label className={'m-0'} htmlFor={'termAndConditionId'}>
                مجوز انتشار
              </Form.Label>
            </div>
          </Form.Group>
        </Row>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>انتخاب مجموعه</Form.Label>
            <Form.Select
              name={'datasetTypeId'}
              className={'mb-1'}
              value={form.datasetTypeId}
              onChange={onHandleFormChange}
              disabled={loaddingType || loaddingAdd || types?.length === 0}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>

              {types?.map((option, index) => (
                <option
                  value={option.datasetTypeId}
                  key={`option_type_${index}`}
                >
                  {option.title}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>کاربرد</Form.Label>
            <Form.Select
              name={'applicationTypeId'}
              className={'mb-1'}
              value={form.applicationTypeId}
              onChange={onHandleFormChange}
              disabled={loaddingType || loaddingAdd}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>
              <option value={0}>پیش‌بینی ریزش مشتری</option>
              <option value={1}>تحلیل رفتار مشترکین</option>
              <option value={2}>اعتبار مشترکین</option>
              <option value={3}>تشخصی ناهنجاری در مصرف</option>
              <option value={4}>تشخیص ناهنجاری در بار مضاعف زیرساخت</option>
              <option value={5}>درک زبان طبیعی</option>
              <option value={6}>پردازش زبان طبیعی</option>
              <option value={7}>چت بات فارسی</option>
              <option value={8}>پیش‌بینی مصرف و پیشنهاد بسته</option>
              <option value={9}>خوشه‌بندی مشتریان</option>
              <option value={10}>تحلیل و عیب‌یابی خطاها</option>
              <option value={11}>تحلیل سلامت شبکه</option>
            </Form.Select>
          </Form.Group>
        </Row>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح مجموعه</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={3}
            name={'datasetDasc'}
            value={form.datasetDasc}
            onChange={onHandleFormChange}
            placeholder={'لطفا شرح مجموعه داده را وارد کنید'}
            disabled={loaddingType || loaddingAdd}
          />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>اسناد مورد نیاز</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={2}
            name={'permissionDasc'}
            value={form.permissionDasc}
            onChange={onHandleFormChange}
            placeholder={'لطفا اسناد مورد نیاز را وارد کنید'}
            disabled={loaddingType || loaddingAdd}
          />
        </Form.Group>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>بارگذاری فایل اکسل مجموعه دادگان</Form.Label>
            <div
              className={'d-flex align-items-center justify-content-between'}
            >
              <Button
                variant={'primary'}
                onClick={() => document.getElementById('fileId').click()}
                disabled={
                  loaddingType ||
                  loaddingAdd ||
                  loadingUpload ||
                  fileFields.fileId ||
                  (fileFields.descFileId && form.descFileId.length === 0)
                }
              >
                انتخاب فایل
              </Button>
              {fileFields.fileId &&
                (form.fileId.length > 0 ? (
                  <div
                    className={'d-flex align-items-center gap-2 text-danger'}
                    role={'button'}
                    onClick={() => onRemoveFile('fileId')}
                  >
                    <FontAwesomeIcon icon={faTimesCircle} size={'sm'} />
                    <span className={'fs-8'}>حذف فایل</span>
                  </div>
                ) : (
                  <div
                    className={'d-flex align-items-center gap-2 text-success'}
                  >
                    <Spinner animation={'border'} size={'sm'} />
                    <span className={'fs-8'}>در حال بارگذاری</span>
                  </div>
                ))}
            </div>
            <Form.Control
              type={'file'}
              accept={'.xlsx,.xls'}
              id={'fileId'}
              name={'fileId'}
              className={'file'}
              onChange={onHandleFile}
              multiple={false}
              hidden={true}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>بارگذاری فایل مستندات</Form.Label>
            <div
              className={'d-flex align-items-center justify-content-between'}
            >
              <Button
                variant={'primary'}
                onClick={() => document.getElementById('descFileId').click()}
                disabled={
                  loaddingType ||
                  loaddingAdd ||
                  loadingUpload ||
                  fileFields.descFileId ||
                  (fileFields.fileId && form.fileId.length === 0)
                }
              >
                انتخاب فایل
              </Button>
              {fileFields.descFileId &&
                (form.descFileId.length > 0 ? (
                  <div
                    className={'d-flex align-items-center gap-2 text-danger'}
                    role={'button'}
                    onClick={() => onRemoveFile('descFileId')}
                  >
                    <FontAwesomeIcon icon={faTimesCircle} size={'sm'} />
                    <span className={'fs-8'}>حذف فایل</span>
                  </div>
                ) : (
                  <div
                    className={'d-flex align-items-center gap-2 text-success'}
                  >
                    <Spinner animation={'border'} size={'sm'} />
                    <span className={'fs-8'}>در حال بارگذاری</span>
                  </div>
                ))}
            </div>
            <Form.Control
              type={'file'}
              accept={'.xlsx,.xls,.csv,.doc,.docx,.pdf,.txt,.png,.jpg,.jpeg'}
              id={'descFileId'}
              name={'descFileId'}
              className={'file'}
              onChange={onHandleFile}
              multiple={false}
              hidden={true}
            />
          </Form.Group>
        </Row>
      </Form>
    ),
    delete: (
      <div>
        شما در حال حذف
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetId)
            )
            .map(item => item.shownName)
            .join(' و ')}
        </span>
        هستید.
        <br />
        با حذف این مجموعه‌ی دادگان، علاوه بر حذف از پنل ادمین، این مجموعه از
        دسترس تمامی کاربران درخواست داده نیز خارج خواهد شد.
      </div>
    ),
    free: isForbidden ? (
      <div>
        <p className={'fs-6 text-soft-primary'}>
          این داده هنوز منتشر نشده است. داده‌ی منتشر نشده امکان آزادسازی ندارد.
        </p>
      </div>
    ) : (
      onGenerateStateBody(3)
    ),
    publish: isForbidden ? (
      <div>
        <p className={'fs-6 text-soft-primary'}>
          این داده هنوز پردازش نشده است. داده‌ی پردازش نشده امکان انتشار ندارد.
        </p>
      </div>
    ) : (
      onGenerateStateBody(2)
    )
  }

  const onGenerateFooter = {
    request: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onSubmitRequest}
          disabled={
            request.goalCat.length === 0 ||
            request.description.length === 0 ||
            request.accept === 0 ||
            loaddingPermission ||
            loadingUpload
          }
        >
          ثبت درخواست
        </Button>
      </>
    ),
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onSubmitDataset}
          disabled={
            form.shownName.length === 0 ||
            form.datasetDasc.length === 0 ||
            form.permissionDasc.length === 0 ||
            form.datasetTypeId.length === 0 ||
            form.applicationTypeId.length === 0 ||
            form.descFileId.length === 0 ||
            form.fileId.length === 0 ||
            loadingUpload ||
            (fileFields.descFileId && form.descFileId.length === 0) ||
            (fileFields.fileId && form.fileId.length === 0) ||
            loaddingType ||
            loaddingAdd
          }
        >
          افزودن دادگان
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'danger'}
          onClick={onDeleteDataset}
          disabled={loaddingDelete}
        >
          حذف دادگان
        </Button>
      </>
    ),
    free: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        {!isForbidden && (
          <Button
            variant={'success'}
            onClick={onUpdateDatasetState}
            disabled={state.datasetStateTypeId === 0 || loadingState}
          >
            آزادسازی دادگان
          </Button>
        )}
      </>
    ),
    publish: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        {!isForbidden && (
          <Button
            variant={'success'}
            onClick={onUpdateDatasetState}
            disabled={state.datasetStateTypeId === 0 || loadingState}
          >
            انتشار دادگان
          </Button>
        )}
      </>
    )
  }

  const onGenerateActions = (
    <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
      {isAdmin ? (
        <>
          <Button variant="success" onClick={() => onShowModal('add')}>
            افزودن دادگان
          </Button>
          {selected.length > 0 && (
            <>
              <Button variant="primary" onClick={onGoToProcess}>
                پردازش
              </Button>
              <Button variant="primary" onClick={onGoToPermissionUsers}>
                کاربران مجاز
              </Button>
              <Button
                variant="soft-primary"
                onClick={() => onShowModal('free')}
              >
                آزادسازی
              </Button>
              <Button
                variant="soft-primary"
                onClick={() => onShowModal('publish')}
              >
                انتشار
              </Button>
              <Button variant="danger" onClick={() => onShowModal('delete')}>
                حذف
              </Button>
            </>
          )}
        </>
      ) : (
        <>
          {selected.length > 0 && (
            <Button variant="success" onClick={() => onShowModal('request')}>
              درخواست
            </Button>
          )}
        </>
      )}

      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
        <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
      </Modal>
    </div>
  )

  return (
    <Table
      items={items}
      loading={loadingItems}
      columns={DATASET.COLUMNS}
      checkedItems={selected}
      onChecked={onHandleSelected}
      isAdmin={isAdmin}
      actions={onGenerateActions}
      searchKey={'shownName'}
    />
  )
}
