import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Image,
  Row,
  Spinner
} from '@themesberg/react-bootstrap'
import Logo from 'assets/images/logo-light.png'
import useCountDown from 'hooks/useCountDown'
import useHttp from 'hooks/useHttp'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { toEnglish, toPersian } from 'src/helper/formatter'
import { setToken } from 'store/slices/UserSlice'

export default () => {
  const dispatch = useDispatch()
  const [step, setStep] = useState(1)
  const [data, setData] = useState({ mobileNumber: '', code: '' })
  const [maxLength, setMaxLength] = useState(11)
  const [isLoadingM, responseM, errorM, onRequestM] = useHttp()
  const [isLoadingC, responseC, errorC, onRequestC] = useHttp()
  const [, , timer, isEnded, isStopped, onStart, onStop, onReset] =
    useCountDown({ m: 1 })

  useEffect(() => {
    setMaxLength(data.mobileNumber.startsWith('0') ? 11 : 10)
    return () => {
      setMaxLength(11)
    }
  }, [data.mobileNumber])

  useEffect(() => {
    if (responseM?.isSuccess) {
      setTimeout(() => {
        setStep(2)
      }, 1000)
    }
    if (errorM) {
      console.warn('Error: AuthWithMobileNumber', errorM)
    }
  }, [responseM, errorM])

  useEffect(() => {
    if (responseC?.isSuccess) {
      setStep(3)
      setTimeout(() => {
        dispatch(setToken(responseC?.value?.token))
      }, 1000)
      console.info('Response: VerifyAuthWithMobileNumber', responseC)
    }
    if (errorC) {
      console.warn('Error: VerifyAuthWithMobileNumber', errorC)
    }
  }, [responseC, errorC, dispatch])

  useEffect(() => {
    if (step === 2) {
      onStart()
    } else if (step !== 2 && !isStopped) {
      onStop()
    }

    return () => {
      onStop()
    }
  }, [step, onStart, isStopped, onStop])

  const onHandleChange = event => {
    const { name, value } = event.target
    setData({ ...data, [name]: toEnglish(value).replace(/\D/, '') })
  }

  const onSubmitMobile = event => {
    const { name } = event.target
    event.preventDefault()
    if (name === 'reset') {
      setData({ mobileNumber: '', code: '' })
      setTimeout(() => {
        setStep(1)
      }, 500)
    } else if (name === 'resend') {
      onRequestM('AuthWithMobileNumber', { mobileNumber: data.mobileNumber })
      onReset()
    } else if (step === 1 || name === 'resend') {
      onRequestM('AuthWithMobileNumber', { mobileNumber: data.mobileNumber })
    } else {
      onRequestC('VerifyAuthWithMobileNumber', {
        mobileNumber: data.mobileNumber,
        code: data.code
      })
    }
  }

  return (
    <div
      className={
        'bg-login d-flex align-items-center justify-content-center min-vh-100 p-5 bg-soft-primary'
      }
    >
      <Container fluid>
        <Card bg={'white'} className={'p-3 shadow w-100 fmxw-400 mx-auto'}>
          <Card.Body
            as={'form'}
            autoComplete={'off'}
            className={'d-flex flex-column align-items-stretch gap-3'}
            onSubmit={onSubmitMobile}
          >
            <Image src={Logo} className={'mx-auto mb-3'} />

            {step === 1 && (
              <>
                <div
                  className={'fs-7 lh-lg fw-medium'}
                  style={{ wordSpacing: '-2px' }}
                >
                  به سکوی دادگان آزادشده همراه اول خوش‌ آمدید
                  <br />
                  لطفا برای دسترسی به دادگان شماره موبایل خود را وارد کنید
                </div>
                <Form.Group
                  className={'form-floating'}
                  controlId={'mobileNumber'}
                >
                  <Form.Control
                    name={'mobileNumber'}
                    type={'tel'}
                    size={'lg'}
                    placeholder={'شماره موبایل را وارد کنید'}
                    autoComplete={'off'}
                    className={'text-black fw-medium text-ltr text-center ls-3'}
                    maxLength={maxLength}
                    onChange={onHandleChange}
                    value={toPersian(data.mobileNumber)}
                  />
                  <Form.Label className={'m-0'}>شماره موبایل</Form.Label>
                </Form.Group>

                <Button
                  variant={'success'}
                  size={'lg'}
                  type={'submit'}
                  className={'fw-medium fh-50 fs-7'}
                  disabled={
                    data.mobileNumber.length !== maxLength ||
                    isLoadingM ||
                    responseM
                  }
                >
                  {responseM || isLoadingM ? (
                    <Spinner animation={'border'} size={'sm'} />
                  ) : (
                    'ارسال پیامک'
                  )}
                </Button>
              </>
            )}

            {step === 2 && (
              <>
                <Row className={'pt-2 px-2'}>
                  <Col xs={12} className={'px-1 pb-2'}>
                    <div className={'w-100 text-center fs-6 fw-normal'}>
                      {data.mobileNumber.length === maxLength ? (
                        <>
                          شما در حال ورود با شماره
                          <span className={'fw-medium mx-2'}>
                            {toPersian(data.mobileNumber)}
                          </span>
                          هستید
                        </>
                      ) : (
                        'تغییر شماره موبایل...'
                      )}
                    </div>
                  </Col>
                  <Col xs={12} className={'px-1 text-center'}>
                    <Button
                      name={'reset'}
                      variant={'soft-primary'}
                      size={'lg'}
                      className={'fw-normal w-50 fs-7 p-2'}
                      onClick={onSubmitMobile}
                      disabled={data.mobileNumber.length !== maxLength}
                    >
                      {data.mobileNumber.length === maxLength ? (
                        'تغییر شماره موبایل'
                      ) : (
                        <Spinner animation={'border'} size={'sm'} />
                      )}
                    </Button>
                  </Col>
                </Row>
                <div
                  className={'fs-7 lh-lg fw-medium'}
                  style={{ wordSpacing: '-2px' }}
                >
                  کد ارسال شده به موبایل خود را وارد کنید
                </div>

                <Form.Group className={'form-floating'} controlId={'code'}>
                  <Form.Control
                    name={'code'}
                    type={'tel'}
                    size={'lg'}
                    placeholder={'کد ارسال شده به موبایل را وارد کنید'}
                    autoComplete={'off'}
                    className={'text-black fw-medium text-ltr text-center ls-3'}
                    maxLength={6}
                    onChange={onHandleChange}
                    value={toPersian(data.code)}
                    isInvalid={data.code.length === 6 && errorC}
                  />
                  <Form.Label className={'m-0'}>
                    کد ارسال شده به موبایل
                  </Form.Label>
                </Form.Group>

                <Button
                  variant={'success'}
                  size={'lg'}
                  type={'submit'}
                  className={'fw-medium fh-50 fs-7'}
                  disabled={data.code.length !== 6 || isLoadingC || responseC}
                >
                  {responseC || isLoadingC ? (
                    <Spinner animation={'border'} size={'sm'} />
                  ) : (
                    'ورود به پنل'
                  )}
                </Button>

                <Button
                  name={'resend'}
                  variant={'outline-info'}
                  size={'lg'}
                  className={'fw-medium fs-7'}
                  disabled={!isEnded}
                  onClick={onSubmitMobile}
                >
                  {isEnded
                    ? 'ارسال مجدد کد'
                    : `${toPersian(timer)} تا ارسال مجدد`}
                </Button>
              </>
            )}

            {step === 3 && (
              <Spinner
                animation={'border'}
                size={'lg'}
                className={'mx-auto my-5'}
              />
            )}
          </Card.Body>
        </Card>
      </Container>
    </div>
  )
}
