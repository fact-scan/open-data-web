import {
  Button,
  Col,
  Form,
  ProgressBar,
  Row,
  Spinner
} from '@themesberg/react-bootstrap'
import { defaultFilter, downloadLink } from 'constants/defaults'
import { PROCESS } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'
import 'scss/dataset.scss'
import { toPersian } from 'src/helper/formatter'

const STATUS_MAP = {
  C00: { status: 'disabled', isDisabled: true, isChecked: false },
  C01: { status: 'required', isDisabled: true, isChecked: true },
  C10: { status: 'selected', isDisabled: false, isChecked: true },
  C11: { status: 'default', isDisabled: false, isChecked: true },
  C22: { status: 'empty', isDisabled: false, isChecked: false }
}

const STATUS_MAP_REVERSE = {
  true: {
    disabled: { isAccess: false, isRequired: false },
    required: { isAccess: false, isRequired: true },
    selected: { isAccess: true, isRequired: false },
    default: { isAccess: true, isRequired: true },
    empty: { isAccess: null, isRequired: null }
  },
  false: {
    disabled: { isAccess: false, isRequired: false },
    required: { isAccess: false, isRequired: true },
    selected: { isAccess: true, isRequired: true },
    default: { isAccess: true, isRequired: true },
    empty: { isAccess: null, isRequired: null }
  }
}

// DESCRIPTION: 2 Minutes Dealy / 15 Seconds Request Delay
const MIN_TIME = (2 * 60 * 1000) / 100
// const MAX_TIME = (10 * 60 * 1000) / 100
const DELAY_TIME = 15 * 1000 // 2 Minutes

export default props => {
  const { isAlias } = props
  const { duid, tuid } = useParams()
  const [now, setNow] = useState(-1)
  const [intervalId, setIntervalId] = useState(0)
  const [fileIntervalId, setFileIntervalId] = useState(0)
  const [filter] = useState({ ...defaultFilter, datasetTypeId: tuid })
  const [features, setFeatures] = useState([])
  const [operations, setOperations] = useState([])
  const [cells, setCells] = useState([])
  const [selected, setSelected] = useState([])
  const [isOperation, setIsOperation] = useState(false)
  const [loadingOperations, , , onRequestOperations] = useHttp()
  const [loadingFeatures, , , onRequestFeatures] = useHttp()
  const [loadingCells, , , onRequestCells] = useHttp()
  const [loadingSubmit, , , onRequestSubmit] = useHttp()
  const [, responseDataset, , onRequestDataset] = useHttp()
  const [, , , onRequestState] = useHttp()
  const [refetch, setRefetch] = useState(true)

  useEffect(() => {
    if (refetch && filter) {
      setRefetch(false)
      const featurePromise = new Promise(async resolve => {
        const response = await onRequestFeatures(
          'GetAllFeatureForAdmin',
          filter
        )
        resolve(response?.value?.dataList)
      })
      const operationPromise = new Promise(async resolve => {
        const response = await onRequestOperations(
          'GetAllOperationForAdmin',
          filter
        )
        resolve(response?.value?.dataList)
      })
      const cellPromise = new Promise(async resolve => {
        const response = await onRequestCells(
          'GetAllRellFeatureOperationForAdmin',
          filter
        )
        resolve(response?.value?.dataList)
      })

      Promise.all([featurePromise, operationPromise, cellPromise]).then(
        response => {
          setFeatures([...response[0]])
          setOperations([...response[1]])
          setCells([
            ...response[2].map(
              ({
                operationId,
                featureId,
                isAccess,
                isRequired,
                rellFeatureOperationId
              }) => {
                const CELL =
                  STATUS_MAP[`C${isAccess ? 1 : 0}${isRequired ? 1 : 0}`]
                return {
                  cell: `${operationId}@${featureId}`,
                  status: CELL.status,
                  disabled: CELL.isDisabled,
                  checked: CELL.isChecked,
                  action: rellFeatureOperationId,
                  test: `C${isAccess ? 1 : 0}${isRequired ? 1 : 0}`
                }
              }
            )
          ])
        }
      )
    }
  }, [onRequestFeatures, onRequestOperations, onRequestCells, filter, refetch])

  useEffect(() => {
    setSelected([...cells])
  }, [cells])

  useEffect(() => {
    if (
      now === 101 &&
      intervalId &&
      fileIntervalId &&
      (!!responseDataset?.value?.processedFileId ||
        !!responseDataset?.value?.fileId)
    ) {
      clearInterval(fileIntervalId)
      clearInterval(intervalId)
      setIntervalId(0)
    }

    if (!!responseDataset?.value?.processedFileId) {
      onRequestState('UpdateDatasetState', {
        datasetStateTypeId: 1
      }).then(response => {
        if (response?.isSuccess) {
          setNow(101)
        }
      })
    }
  }, [now, intervalId, fileIntervalId, responseDataset, onRequestState])

  const onStartProgress = useCallback(() => {
    const newFileIntervalId = setInterval(() => {
      const body = {
        datasetId: duid
      }
      onRequestDataset('GetDataset', body)
    }, DELAY_TIME)

    const newIntervalId = setInterval(() => {
      setNow(prev => (prev >= 100 ? 101 : prev + 1))
    }, MIN_TIME)

    setFileIntervalId(newFileIntervalId)
    setIntervalId(newIntervalId)
  }, [onRequestDataset, duid])

  const onSubmitCells = useCallback(
    async (isStart = false) => {
      setIsOperation(isStart)
      const body = {
        rellFeatureOperations: selected.map(item => ({
          featureId: item.cell.split('@')[1],
          operationId: item.cell.split('@')[0],
          rellFeatureOperationId: item.action,
          ...STATUS_MAP_REVERSE[isStart][item.status]
        })),
        datasetTypeId: tuid,
        datasetId: duid,
        startOperation: isStart
      }

      if (isStart) setNow(0)
      const response = await onRequestSubmit(
        'AddOrUpdateRellFeatureOperation',
        body
      )

      if (!isStart) {
        setRefetch(true)
      } else if (response?.isSuccess && isStart) {
        onStartProgress()
      }
    },
    [onRequestSubmit, tuid, duid, selected, onStartProgress]
  )

  const onGenerateCategory = useMemo(() => {
    const category = []
    PROCESS.CATEGORY.forEach(item => {
      const cols = operations.filter(op => op.category === item.id).length || 0
      const rows = item.id < 0 ? 3 : 1
      category.push({
        title: item.title,
        cols,
        rows
      })
    })

    return (
      <tr className={'head category'}>
        {category.map(({ title, cols, rows }, i) => (
          <td className={'cell'} key={`ci_${i}`} rowSpan={rows} colSpan={cols}>
            <span className={'text'}>{title}</span>
          </td>
        ))}
      </tr>
    )
  }, [operations])

  const onGenerateSubCategory = useMemo(() => {
    const subcategory = []
    PROCESS.CATEGORY.forEach(item => {
      const category = operations.filter(op => op.category === item.id)
      category.forEach(op => {
        subcategory.push({
          title: op.subCategory,
          cols: 1
        })
      })
    })

    return (
      <tr className={'head subcategory'}>
        {subcategory.map(({ title, cols }, i) => (
          <td className={'cell'} key={`si_${i}`} colSpan={cols}>
            <span className={'text'}>{title}</span>
          </td>
        ))}
      </tr>
    )
  }, [operations])

  const onGenerateOperation = useMemo(() => {
    const operation = []
    PROCESS.CATEGORY.forEach(item => {
      const category = operations.filter(op => op.category === item.id)
      category.forEach(op => {
        operation.push({
          title: op.shownName,
          cols: 1
        })
      })
    })

    return (
      <tr className={'head operation'}>
        {operation.map(({ title, cols }, i) => (
          <td className={'cell'} key={`oi_${i}`} colSpan={cols}>
            <span className={'text'}>{title}</span>
          </td>
        ))}
      </tr>
    )
  }, [operations])

  const onGenerateFeatureOperation = useMemo(() => {
    const operation = []
    PROCESS.CATEGORY.forEach(item => {
      const category = operations.filter(op => op.category === item.id)
      category.forEach(op => {
        operation.push({
          creatorId: op.creatorId,
          operationId: op.operationId,
          cols: 1
        })
      })
    })

    const onCheckHandle = event => {
      const { value, checked } = event.target

      const newSelected = checked
        ? [
            ...selected,
            {
              cell: value,
              status: STATUS_MAP['C10'].status,
              disabled: STATUS_MAP['C10'].isDisabled,
              checked: STATUS_MAP['C10'].isChecked,
              action: cells.find(item => item.cell === value)?.action || 0
            }
          ]
        : selected.filter(item => item.cell !== value)
      setSelected([...newSelected])
    }

    return features.map(({ datasetColumn: { title }, featureId }, i) => (
      <tr className={'item'} key={`fi_${i}`}>
        <td className={`cell${now >= 0 ? ' in-progress' : ''}`} colSpan={0}>
          <span className={'text'}>{title || '-'}</span>
        </td>

        {operation.map(({ operationId, cols }, j) => {
          const identifier = `${operationId}@${featureId}`
          const current =
            selected.find(c => c.cell === identifier) || STATUS_MAP['C22']

          return (
            current && (
              <td
                className={`cell${now >= 0 ? ' in-progress' : ''}`}
                key={`foi_${j}`}
                colSpan={cols}
              >
                <Form.Check
                  label={''}
                  type={'checkbox'}
                  name={identifier}
                  id={identifier}
                  onChange={onCheckHandle}
                  defaultValue={identifier}
                  disabled={now >= 0 || loadingSubmit || current.disabled}
                  defaultChecked={current.checked}
                  className={`checkbox ${current.status}${
                    now >= 0 ? ' in-progress' : ''
                  }`}
                />
              </td>
            )
          )
        })}
      </tr>
    ))
  }, [operations, features, selected, cells, loadingSubmit, now])

  const onGenerateFooter = useMemo(() => {
    const percent = now > 0 ? (now > 100 ? 100 : now) : 0
    const isOnProgress = (0 <= now && now <= 100) || loadingSubmit
    const isOnStart = !loadingSubmit && now < 0
    const isOnFinish = !loadingSubmit && now > 100
    const isLinkReady =
      isOnFinish &&
      (responseDataset?.value?.processedFileId ||
        responseDataset?.value?.fileId)

    const onResetCells = () => {
      setSelected([...cells])
    }

    return (
      <>
        {(isOnProgress || isOnFinish) && !isAlias && (
          <ProgressBar
            animated
            now={percent}
            label={`${toPersian(percent)}%`}
            style={{ height: '32px', maxWidth: '350px', direction: 'ltr' }}
            className={'flex-fill bg-white rounded fs-7 fw-normal lh-1 mb-0'}
            variant={'success'}
          />
        )}
        <div />
        <div className={'d-flex align-items-center justify-content-end gap-2'}>
          {isOnStart && !isAlias && (
            <Button variant={'danger'} onClick={onResetCells}>
              بازنشانی به حالت پیش‌فرض
            </Button>
          )}

          {isLinkReady && !isAlias && (
            <form
              method={'get'}
              target={'_blank'}
              action={`${downloadLink}/${
                responseDataset?.value?.processedFileId ||
                responseDataset?.value?.fileId
              }`}
            >
              <Button variant={'soft-primary'} type={'submit'}>
                دریافت فایل
              </Button>
            </form>
          )}

          {(isOnStart || isOnProgress || !isLinkReady) && !isAlias && (
            <>
              <Button
                variant={'soft-primary'}
                onClick={() => onSubmitCells(false)}
                disabled={isOnProgress || isOnFinish}
              >
                ذخیره به عنوان پیش فرض
              </Button>
              <Button
                variant={'success'}
                onClick={() => onSubmitCells(true)}
                disabled={isOnProgress || isOnFinish}
              >
                {!isOperation || isOnStart
                  ? 'شروع عملیات'
                  : 'در حال پردازش ...'}
              </Button>
            </>
          )}
        </div>
      </>
    )
  }, [
    now,
    isAlias,
    loadingSubmit,
    isOperation,
    cells,
    onSubmitCells,
    responseDataset
  ])

  return (
    <div
      className={
        'd-flex flex-column align-items-stretch justify-content-start mt-5 mb-3 flex-grow-1'
      }
    >
      <Row className={'mb-4'}>
        <Col xs={{ span: 8 }}>
          <div className={'mb-2'}>
            با کلیک بر روی هر کدام از سلول‌ها عملیات مرتبط را برای ویژگی مربوطه
            برنامه‌ریزی می‌کنید.
          </div>
          <div>
            پس از کلیک بر روی دکمه «شروع عملیات» عملیات‌های انتخاب شده به ازای
            ویژگی شروع به اعمال شدن خواهند کرد.
          </div>
        </Col>
        <Col>
          <div className={'table-info'}>
            <div className={'info-item'}>
              <span className={'icon required'} />
              <span className={'text'}>عملیات لازم برای همه داده‌ها</span>
            </div>
            <div className={'info-item'}>
              <span className={'icon disabled'} />
              <span className={'text'}>عملیات غیرقابل انجام</span>
            </div>
            <div className={'info-item'}>
              <span className={'icon default'} />
              <span className={'text'}>عملیات پیش‌فرض این مجموعه</span>
            </div>
            <div className={'info-item'}>
              <span className={'icon selected'} />
              <span className={'text'}>عملیات انتخاب شده توسط شما</span>
            </div>
          </div>
        </Col>
      </Row>

      <div className={'overflow-hidden process-wrapper mb-4'}>
        {loadingFeatures || loadingOperations || loadingCells ? (
          <div className={'process-loader gap-2'}>
            <Spinner animation={'border'} variant={'soft-primary'} />
            <span className={'fs-6 fw-medium text-soft-primary'}>
              در حال دریافت اطلاعات
            </span>
          </div>
        ) : (
          <table
            className={`process${
              now >= 0 ? (now === 101 ? ' is-progressed' : ' in-progress') : ''
            }`}
          >
            <thead>
              {onGenerateCategory}
              {onGenerateSubCategory}
              {onGenerateOperation}
            </thead>
            <tbody>{onGenerateFeatureOperation}</tbody>
          </table>
        )}
      </div>

      <div className={'d-flex align-items-center justify-content-between'}>
        {onGenerateFooter}
      </div>
    </div>
  )
}
