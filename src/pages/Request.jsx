import { faDownload } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Form, Modal } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { defaultFilter, downloadLink } from 'constants/defaults'
import { REQUEST } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import React, { useCallback, useEffect, useState } from 'react'

const DEFAULTS = {
  form: {
    datasetPermissionRequestId: null,
    response: '',
    state: 2
  }
}

export default props => {
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)
  const [items, setItems] = useState([])
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [formData, setFormData] = useState(null)
  const [loadingItems, responseItems, errorItems, onRequestItems] = useHttp()
  const [loadingState, , , onRequestState] = useHttp()
  const [loadingData, , , onRequestData] = useHttp()
  const [loaddingDelete, , , onRequestDelete] = useHttp()

  const onGetItems = useCallback(() => {
    onRequestItems(
      isAdmin
        ? 'GetAllPermissionRequestForAdmin'
        : 'GetAllPermissionRequestForMe',
      filter
    )
  }, [onRequestItems, filter, isAdmin])

  const onChangeRequestState = async () => {
    const response = await onRequestState('ChangePermissionRequestState', {
      datasetPermissionRequestId: items.find(item =>
        selected
          .map(sitem => JSON.parse(sitem.split('|')[0]))
          .includes(item.datasetPermissionRequestId)
      )?.datasetPermissionRequestId,
      response: form.response || '',
      state:
        isModal === 'confirm'
          ? 4
          : isModal === 'reject'
          ? 5
          : form.state > 3
          ? form.state
          : form.response.length > 0
          ? 3
          : 2
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onDeleteRequest = async () => {
    const response = await onRequestDelete('DeletePermissionRequest', {
      datasetPermissionRequestId: items.find(item =>
        selected
          .map(sitem => JSON.parse(sitem.split('|')[0]))
          .includes(item.datasetPermissionRequestId)
      )?.datasetPermissionRequestId
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  useEffect(() => {
    onGetItems()
  }, [onGetItems])

  useEffect(() => {
    if (responseItems?.isSuccess) {
      setItems([...responseItems?.value?.dataList])
    }
    if (errorItems?.isFailed) {
      console.warn(
        `Error: ${
          isAdmin
            ? 'GetAllPermissionRequestForAdmin'
            : 'GetAllPermissionRequestForMe'
        }`,
        errorItems
      )
    }
  }, [responseItems, errorItems, isAdmin])

  useEffect(() => {
    if (isModal === 'check' && formData === null) {
      ;(async () => {
        const response = await onRequestData('GetPermissionRequest', {
          datasetPermissionRequestId: items.find(item =>
            selected
              .map(sitem => JSON.parse(sitem.split('|')[0]))
              .includes(item.datasetPermissionRequestId)
          )?.datasetPermissionRequestId
        })
        if (response?.value) {
          setFormData(response?.value)
          setForm({
            ...form,
            response: response?.value?.response || '',
            state: parseInt(response?.value?.state || 2)
          })
        }
      })()
    }
    if (!isModal) {
      setForm(DEFAULTS.form)
      setFormData(null)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModal])

  const onHandleFormChange = event => {
    const { name, value, checked } = event.target
    const newForm =
      isModal === 'check'
        ? {
            normal: { ...form, [name]: value },
            state: {
              ...form,
              [name]: checked ? parseInt(value) : formData?.state || 2
            }
          }
        : {
            normal: { ...form, [name]: value },
            state: {
              ...form,
              [name]: checked ? parseInt(value) : formData?.state || 2
            }
          }

    isModal === 'check'
      ? setForm(newForm[name] || newForm['normal'])
      : setForm(newForm[name] || newForm['normal'])
  }

  const onHandleSelected = checkedItems => {
    setSelected(checkedItems)
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateTitle = {
    check: 'بررسی دسترسی',
    confirm: 'تأیید تعلق دسترسی',
    reject: 'رد تعلق دسترسی',
    add: 'درخواست جدید',
    delete: 'حذف درخواست',
    view: 'مشاهده پاسخ درخواست'
  }

  const onGenerateBody = {
    check: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>هدف از درخواست</Form.Label>
          <Form.Select
            name={'goalCat'}
            className={'mb-1'}
            value={formData?.goalCat || ''}
            disabled={true}
          >
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={0}>استفاده تجاری</option>
            <option value={1}>استفاده تحقیقاتی</option>
            <option value={2}>مسابقه</option>
            <option value={3}>درون سازمانی</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <div className={'fs-6 text-gray-600 fmh-50'}>
            {formData?.description || ''}
          </div>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Button
            onClick={() =>
              window.open(`${downloadLink}/${formData?.fileId}`, '_blank')
            }
            variant={'soft-primary'}
            disabled={!formData?.fileId}
          >
            <FontAwesomeIcon icon={faDownload} pull={'right'} size={'lg'} />
            <span>
              {formData?.fileId
                ? 'دانلود اسناد بارگذاری شده'
                : 'کاربر اسنادی بارگذاری نکرده است'}
            </span>
          </Button>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>پاسخ به درخواست</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={3}
            name={'response'}
            value={form.response}
            onChange={onHandleFormChange}
            placeholder={'لطفا پاسخ درخواست را وارد کنید'}
            disabled={loadingState || loadingData}
          />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <div
            className={'d-flex align-items-center justify-content-start gap-2'}
          >
            <Form.Check
              type={'checkbox'}
              id={'state'}
              name={'state'}
              value={4}
              onChange={onHandleFormChange}
              checked={form.state === 4}
              disabled={loadingState || loadingData}
            />
            <Form.Label className={'m-0'} htmlFor={'state'} role={'button'}>
              تأیید دسترسی پس از ارسال
            </Form.Label>
          </div>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <div
            className={'d-flex align-items-center justify-content-start gap-2'}
          >
            <Form.Check
              type={'checkbox'}
              id={'state'}
              name={'state'}
              value={5}
              onChange={onHandleFormChange}
              checked={form.state === 5}
              disabled={loadingState || loadingData}
            />
            <Form.Label className={'m-0'} htmlFor={'state'} role={'button'}>
              رد دسترسی پس از ارسال
            </Form.Label>
          </div>
        </Form.Group>
      </Form>
    ),
    confirm: (
      <div>
        شما در حال تأیید دسترسی
        <span className={'fw-medium text-success d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(item =>
              item?.user
                ? `${item?.user?.firstName || ''} ${item?.user?.lastName || ''}`
                : 'کاربر بدون نام'
            )
            .join(' و ')}
        </span>
        برای
        <span className={'fw-medium text-success d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(item => item?.dataset?.shownName)
            .join(' و ')}
        </span>
        هستید.
      </div>
    ),
    reject: (
      <div>
        شما در حال رد دسترسی
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(item =>
              item?.user
                ? `${item?.user?.firstName || ''} ${item?.user?.lastName || ''}`
                : 'کاربر بدون نام'
            )
            .join(' و ')}
        </span>
        برای
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(item => item?.dataset?.shownName)
            .join(' و ')}
        </span>
        هستید.
      </div>
    ),
    add: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>مجموعه دادگان</Form.Label>
          <Form.Select className={'mb-1'}>
            <option disabled selected>
              انتخاب کنید
            </option>
            <option value="1">مجموعه دادگان ۱</option>
            <option value="2">مجموعه دادگان ۲</option>
            <option value="3">مجموعه دادگان ۳</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * در صورتی که درخواست شما در مورد مجموعه دادگان خاصی است آن را
            انتخاب کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>دسته‌بندی درخواست</Form.Label>
          <Form.Select className={'mb-1'}>
            <option disabled selected>
              انتخاب کنید
            </option>
            <option value="1">پرسش یا ابهام</option>
            <option value="2">مشکل در دادگان</option>
            <option value="3">مشکل در سیستم</option>
            <option value="3">سایر</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * با انتخاب صحیح دسته‌بندی درخواست پشتیبانی ما را در بهبود سامانه
            یاری کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <Form.Control as={'textarea'} rows={4} />
        </Form.Group>
      </Form>
    ),
    delete: (
      <div>
        شما در حال حذف درخواست دسترسی به
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(item => item?.dataset?.shownName || '')
            .join(' و ')}
        </span>
        هستید.
      </div>
    ),
    view: (
      <div>
        <p className={'fs-6 text-soft-primary'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.datasetPermissionRequestId)
            )
            .map(
              item => item?.response || 'پاسخی برای درخواست شما ثبت نشده است'
            )
            .join(' و ')}
        </p>
      </div>
    )
  }

  const onGenerateFooter = {
    check: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onChangeRequestState}
          disabled={loadingState || loadingData}
        >
          ثبت بررسی درخواست
        </Button>
      </>
    ),
    confirm: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onChangeRequestState}
          disabled={loadingState}
        >
          تأیید درخواست
        </Button>
      </>
    ),
    reject: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'danger'}
          onClick={onChangeRequestState}
          disabled={loadingState}
        >
          رد درخواست
        </Button>
      </>
    ),
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          ثبت درخواست
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'danger'}
          onClick={onDeleteRequest}
          disabled={loaddingDelete}
        >
          حذف درخواست
        </Button>
      </>
    ),
    view: (
      <Button variant={'soft-primary'} onClick={onCloseModal}>
        بستن
      </Button>
    )
  }

  const onGenerateActions = (
    <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
      {isAdmin ? (
        <>
          {selected.length > 0 && (
            <>
              <Button variant="primary" onClick={() => onShowModal('check')}>
                بررسی درخواست
              </Button>
              <Button variant="success" onClick={() => onShowModal('confirm')}>
                تأیید
              </Button>
              <Button variant="danger" onClick={() => onShowModal('reject')}>
                رد
              </Button>
            </>
          )}
        </>
      ) : (
        <>
          {selected.length > 0 && (
            <>
              <Button variant={'primary'} onClick={() => onShowModal('view')}>
                مشاهده پاسخ
              </Button>
              {items
                .filter(item =>
                  selected
                    .map(sitem => JSON.parse(sitem.split('|')[0]))
                    .includes(item.datasetPermissionRequestId)
                )
                .map(item => item?.state)[0] < 4 && (
                <Button
                  variant={'danger'}
                  onClick={() => onShowModal('delete')}
                >
                  حذف درخواست
                </Button>
              )}
            </>
          )}
        </>
      )}

      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
        <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
      </Modal>
    </div>
  )

  return (
    <Table
      items={items}
      loading={loadingItems}
      columns={REQUEST.COLUMNS}
      checkedItems={selected}
      onChecked={onHandleSelected}
      isAdmin={isAdmin}
      actions={onGenerateActions}
      searchKey={'description'}
    />
  )
}
