import {
  faEdit,
  faTrashAlt,
  faUnlock,
  faUsers
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Button,
  Card,
  Col,
  Form,
  ListGroup,
  Modal,
  Row,
  Tab
} from '@themesberg/react-bootstrap'
import { defaultFilter } from 'constants/defaults'
import useHttp from 'hooks/useHttp'
import React, { useEffect, useMemo, useState } from 'react'

const DEFAULTS = {
  form: {
    roleId: null,
    title: '',
    priority: 0
  }
}

export default () => {
  const [roles, setRoles] = useState([])
  const [permissions, setPermissions] = useState([])
  const [data, setData] = useState(null)
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [loadingItems, , , onRequestItems] = useHttp()
  const [loaddingAdd, , , onRequestAdd] = useHttp()
  const [loaddingUpdate, , , onRequestUpdate] = useHttp()
  const [loaddingUpdatePermission, , , onRequestUpdatePermission] = useHttp()
  const [loaddingDelete, , , onRequestDelete] = useHttp()
  const [loaddingPermission, , , onRequestPermission] = useHttp()
  const [loaddingRolePermission, , , onRequestRolePermission] = useHttp()
  const [refetch, setRefetch] = useState(true)
  const [isModal, setIsModal] = useState(false)

  const onSubmitRole = async () => {
    const response = await onRequestAdd('AddRole', form)
    if (response?.isSuccess) {
      setRefetch(true)
      setForm(DEFAULTS.form)
      setData(null)
    }
  }

  const onUpdateRole = async () => {
    const response = await onRequestUpdate('UpdateRole', {
      title: form.title,
      priority: form.priority,
      roleId: form.roleId
    })

    const responsePermission = await onRequestUpdatePermission(
      'AddAndUpdateRangeRellPermissionRole',
      {
        roleId: form.roleId,
        permissionIds: Object.entries(data)
          .filter(item => item[1])
          .map(item => item[0])
      }
    )

    if (response?.isSuccess && responsePermission?.isSuccess) {
      setRefetch(true)
      setForm(DEFAULTS.form)
      setData(null)
    }
  }

  const onDeleteRole = async roleId => {
    const response = await onRequestDelete('DeleteRole', { roleId })
    if (response?.isSuccess) {
      onCloseModal()
      setRefetch(true)
      setForm(DEFAULTS.form)
      setData(null)
    }
  }

  const onCloseModal = () => setIsModal(false)

  useEffect(() => {
    if (refetch && filter) {
      setRefetch(false)

      const rolePromise = new Promise(async resolve => {
        const response = await onRequestItems('GetAllRole', filter)
        resolve(response?.value?.dataList)
      })

      const permissionPromise = new Promise(async resolve => {
        const response = await onRequestPermission('GetAllPermissions', filter)
        resolve(response?.value?.dataList)
      })

      Promise.all([rolePromise, permissionPromise]).then(response => {
        setRoles([...response[0].map(item => ({ ...item, isCollapse: true }))])

        const parents = [
          ...response[1]
            .filter(item => !item.parentId)
            .map(parent => ({
              parentId: parent.permissionId,
              parentTitle: parent.title,
              children: response[1].filter(
                child =>
                  child.parentId === parent.permissionId ||
                  child.permissionId === parent.permissionId
              )
            }))
        ]

        setPermissions(parents)
      })
    }
  }, [onRequestItems, onRequestPermission, filter, refetch])

  const onHandleFormChange = event => {
    const { name, value } = event.target
    const newForm = { ...form, [name]: value }
    setForm(newForm)
  }

  const onGenerateRoles = useMemo(() => {
    const onCheckHandle = event => {
      const { value, checked } = event.target
      const newData = { ...data, [value]: checked }
      setData(newData)
    }

    const onShowModal = id => setIsModal(id)

    const onGetRolePermissions = async roleId => {
      const response = await onRequestRolePermission('GetPermissionRoles', {
        roleId
      })
      if (response?.isSuccess) {
        const onMap = inputArray => {
          const result = []
          inputArray.forEach(item => {
            result.push(
              item.childs?.length
                ? onMap([
                    {
                      permissionId: item.permissionId,
                      checked: item.checked
                    },
                    ...item.childs
                  ])
                : {
                    permissionId: item.permissionId,
                    isChecked: item.checked
                  }
            )
          })

          return result.flat()
        }
        const dataArray = onMap(response.value)
        const newData = {}

        dataArray.forEach((item, index) => {
          Object.assign(newData, {
            [item.permissionId]: item.isChecked
          })
        })

        setData(newData)
      }
    }

    const onToggleCollapse = roleId => {
      const current = roles.find(item => item.roleId === roleId)?.isCollapse
      const newItems = roles.map(item => ({
        ...item,
        isCollapse:
          item.roleId === roleId ? !current : current ? true : item.isCollapse
      }))
      const editedItem = newItems.find(item => !item.isCollapse)
      setRoles(newItems)
      if (editedItem) {
        setForm(editedItem)
        onGetRolePermissions(roleId)
      } else {
        setForm(DEFAULTS.form)
        setData(null)
      }
    }

    return roles.map(({ title: roleTitle, roleId, isCollapse }) => {
      const roleKey = `${roleId}#role`

      return (
        <Card
          key={roleKey}
          className={'w-100'}
          bg={roleId === form.roleId ? 'white' : 'light'}
        >
          <Card.Header
            className={`d-flex align-items-center justify-content-between p-2 pe-3 roles-header`}
          >
            <div
              className={
                'd-flex align-items-center justify-content-between gap-1'
              }
            >
              <FontAwesomeIcon icon={faUsers} size={'lg'} pull={'right'} />
              <span className={'fs-6 fw-bold text-soft-primary'}>
                {roleTitle}
              </span>
            </div>

            <div
              className={
                'd-flex align-items-center justify-content-center gap-2'
              }
            >
              <Button
                variant={roleId === form.roleId ? 'soft-primary' : 'primary'}
                onClick={() => onToggleCollapse(roleId)}
              >
                <FontAwesomeIcon icon={faEdit} size={'lg'} pull={'right'} />
                {roleId === form.roleId ? 'لغو ویرایش' : 'ویرایش'}
              </Button>
              <Button
                variant={'danger'}
                onClick={() => onShowModal(roleId)}
                disabled={roleId === form.roleId}
              >
                <FontAwesomeIcon icon={faTrashAlt} size={'lg'} pull={'right'} />
                حذف
              </Button>
            </div>
          </Card.Header>

          {!isCollapse && (
            <Card.Body className={'roles-body p-3'}>
              <Tab.Container defaultActiveKey={permissions[0]?.parentId}>
                <Row>
                  <Col sm={3}>
                    <ListGroup>
                      {permissions.map(({ parentId, parentTitle }) => {
                        const key = `${parentId}@${roleId}#tab`

                        return (
                          <ListGroup.Item
                            key={key}
                            eventKey={parentId}
                            action={true}
                          >
                            <FontAwesomeIcon icon={faUnlock} size={'lg'} />
                            {parentTitle}
                          </ListGroup.Item>
                        )
                      })}
                    </ListGroup>
                  </Col>
                  <Col sm={9}>
                    <Tab.Content className={'py-3 px-1'}>
                      {permissions.map(({ parentId, children }) => {
                        const key = `${parentId}@${roleId}#content`

                        return (
                          <Tab.Pane
                            key={key}
                            eventKey={parentId}
                            as={ListGroup}
                          >
                            {children.map(({ permissionId, title }) => {
                              const identifier = `${permissionId}@${roleId}`

                              return (
                                data && (
                                  <ListGroup.Item key={identifier}>
                                    <Form.Check
                                      label={title || '-'}
                                      type={'checkbox'}
                                      name={identifier}
                                      id={identifier}
                                      onChange={onCheckHandle}
                                      defaultValue={permissionId}
                                      disabled={loaddingRolePermission}
                                      defaultChecked={data[permissionId]}
                                    />
                                  </ListGroup.Item>
                                )
                              )
                            })}
                          </Tab.Pane>
                        )
                      })}
                    </Tab.Content>
                  </Col>
                </Row>
              </Tab.Container>
            </Card.Body>
          )}
        </Card>
      )
    })
  }, [
    form.roleId,
    roles,
    permissions,
    loaddingRolePermission,
    onRequestRolePermission,
    data
  ])

  return (
    <div
      className={
        'd-flex flex-column align-items-center justify-content-start gap-3 mt-5 mb-3 position-relative'
      }
    >
      <div
        className={`mc-loader roles${
          loaddingAdd ||
          loadingItems ||
          loaddingPermission ||
          loaddingDelete ||
          loaddingUpdate ||
          loaddingUpdatePermission
            ? ' loading'
            : ''
        }`}
      />

      <div
        className={
          'd-flex align-items-center justify-content-start gap-3 w-100'
        }
      >
        <Row className={'w-50 align-items-end gap-2 mx-0'}>
          <Form.Group as={Col} controlId={'role'} className={'px-0'}>
            <Form.Label>نام نقش</Form.Label>
            <Form.Control
              type={'text'}
              name={'title'}
              value={form.title}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام نقش را وارد کنید'}
              disabled={
                loaddingAdd ||
                loadingItems ||
                loaddingPermission ||
                loaddingDelete ||
                loaddingUpdate ||
                loaddingUpdatePermission
              }
            />
          </Form.Group>

          <Form.Group
            as={Col}
            xs={2}
            controlId={'role'}
            className={'px-0'}
            hidden={true}
          >
            <Form.Label>اولویت</Form.Label>
            <Form.Control
              type={'text'}
              name={'priority'}
              value={form.priority}
              onChange={onHandleFormChange}
              placeholder={'0'}
              maxLength={2}
              disabled={
                loaddingAdd ||
                loadingItems ||
                loaddingPermission ||
                loaddingDelete ||
                loaddingUpdate ||
                loaddingUpdatePermission
              }
            />
          </Form.Group>

          <Form.Group
            as={Col}
            controlId={'new-role'}
            className={'px-0'}
            xs={'auto'}
          >
            <Button
              variant={'success'}
              onClick={!!form.roleId ? onUpdateRole : onSubmitRole}
              disabled={
                form.title.length === 0 ||
                loaddingAdd ||
                loadingItems ||
                loaddingPermission ||
                loaddingDelete ||
                loaddingUpdate ||
                loaddingUpdatePermission
              }
            >
              {!!form.roleId ? 'ثبت ویرایش نقش' : 'افزودن نقش'}
            </Button>
          </Form.Group>
        </Row>
      </div>
      {onGenerateRoles}

      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>حذف نقش</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          شما در حال حذف نقش
          <span className={'fw-medium text-danger d-inline-flex mx-2'}>
            {roles.find(item => item.roleId === isModal)?.title || ''}
          </span>
          هستید.
        </Modal.Body>
        <Modal.Footer>
          <Button variant={'soft-primary'} onClick={onCloseModal}>
            بستن
          </Button>
          <Button
            variant={'danger'}
            onClick={() => onDeleteRole(isModal)}
            disabled={loaddingDelete}
          >
            حذف درخواست
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
