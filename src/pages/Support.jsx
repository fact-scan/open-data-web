import { Button, Col, Form, Modal, Row } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { defaultFilter } from 'constants/defaults'
import { SUPPORT } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import React, { useCallback, useEffect, useState } from 'react'

const DEFAULTS = {
  form: {
    datasetId: '',
    description: '',
    response: '',
    categoryId: ''
  }
}

const MAP_STATUS = {
  0: 'پرسش و پیشنهاد',
  1: 'گزارش ایراد داده',
  2: 'گزارش ایراد سیستم',
  3: 'سایر'
}

export default props => {
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)
  const [items, setItems] = useState([])
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [types, setTypes] = useState(null)
  const [loadingItems, responseItems, errorItems, onRequestItems] = useHttp()
  const [loaddingAdd, , , onRequestAdd] = useHttp()
  const [loaddingUpdate, , , onRequestUpdate] = useHttp()
  const [loaddingType, , , onRequestType] = useHttp()
  const [loadingData, , , onRequestData] = useHttp()
  const [loaddingDelete, , , onRequestDelete] = useHttp()

  const onGetItems = useCallback(() => {
    onRequestItems(
      isAdmin ? 'GetAllSupportRequestForAdmin' : 'GetAllSupportRequestForMe',
      filter
    )
  }, [onRequestItems, filter, isAdmin])

  const onSubmitRequest = async () => {
    const response = await onRequestAdd('AddSupportRequest', {
      datasetId: form.datasetId,
      description: form.description,
      categoryId: form.categoryId
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onUpdateRequest = async () => {
    const ids = selected[0].split('|')
    const supportRequestId = JSON.parse(ids[0])
    const response = await onRequestUpdate('UpdateSupportRequest', {
      datasetId: form.datasetId,
      description: form.description,
      categoryId: form.categoryId,
      response: form.response,
      supportRequestId
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onDeleteRequest = async () => {
    const ids = selected[0].split('|')
    const supportRequestId = JSON.parse(ids[0])
    const response = await onRequestDelete('DeleteSupportRequest', {
      supportRequestId
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  useEffect(() => {
    onGetItems()
  }, [onGetItems])

  useEffect(() => {
    if (responseItems?.isSuccess) {
      setItems([...responseItems?.value?.dataList])
    }
    if (errorItems?.isFailed) {
      console.warn(
        `Error: ${
          isAdmin ? 'GetAllSupportRequestForAdmin' : 'GetAllSupportRequestForMe'
        }`,
        errorItems
      )
    }
  }, [responseItems, errorItems, isAdmin])

  useEffect(() => {
    if (isModal === 'add' && types === null) {
      ;(async () => {
        const response = await onRequestType('GetAllDataset', filter)
        if (response?.value?.dataList?.length) {
          setTypes(response?.value?.dataList)
        } else {
          setTypes([])
        }
      })()
    }
    if (isModal === 'check') {
      ;(async () => {
        const ids = selected[0].split('|')
        const supportRequestId = JSON.parse(ids[0])
        const response = await onRequestData('GetSupportRequestForAdmin', {
          supportRequestId
        })
        if (response?.value) {
          setForm({
            datasetId: response?.value?.datasetId,
            description: response?.value?.description,
            categoryId: response?.value?.categoryId,
            response: response?.value?.response || ''
          })
        }
      })()
    }

    if (!isModal) {
      setForm(DEFAULTS.form)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModal])

  const onHandleFormChange = event => {
    const { name, value } = event.target
    const newForm = {
      normal: { ...form, [name]: value },
      categoryId: { ...form, [name]: parseInt(value) }
    }
    setForm(newForm[name] || newForm['normal'])
  }

  const onHandleSelected = checkedItems => {
    setSelected(checkedItems)
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateTitle = {
    add: 'درخواست جدید',
    check: 'بررسی درخواست',
    delete: 'حذف درخواست',
    view: 'مشاهده پاسخ درخواست'
  }

  const onGenerateBody = {
    add: (
      <Form>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>مجموعه دادگان</Form.Label>
            <Form.Select
              name={'datasetId'}
              className={'mb-1'}
              value={form.datasetId}
              onChange={onHandleFormChange}
              disabled={loaddingType || loaddingAdd || types?.length === 0}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>

              {types?.map((option, index) => (
                <option
                  value={option.datasetId}
                  key={`option_dataset_${index}`}
                >
                  {option.shownName}
                </option>
              ))}
            </Form.Select>
            <p className={'text-muted fs-8 text-center w-100 mt-2 mb-0'}>
              * در صورتی که درخواست شما در مورد مجموعه دادگان خاصی است آن را
              انتخاب کنید.
            </p>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>دسته‌بندی درخواست</Form.Label>
            <Form.Select
              name={'categoryId'}
              className={'mb-1'}
              value={form.categoryId}
              onChange={onHandleFormChange}
              disabled={loaddingType || loaddingAdd}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>
              <option value={0}>پرسش و پیشنهاد</option>
              <option value={1}>گزارش ایراد داده</option>
              <option value={2}>گزارش ایراد سیستم</option>
              <option value={3}>سایر</option>
            </Form.Select>
            <p className={'text-muted fs-8 text-center w-100 mt-2 mb-0'}>
              * با انتخاب صحیح دسته‌بندی درخواست پشتیبانی ما را در بهبود سامانه
              یاری کنید.
            </p>
          </Form.Group>
        </Row>

        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={4}
            name={'description'}
            value={form.description}
            onChange={onHandleFormChange}
            placeholder={'لطفا شرح درخواست خود را وارد کنید'}
            disabled={loaddingType || loaddingAdd}
          />
        </Form.Group>
      </Form>
    ),
    check: (
      <Form>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>مجموعه دادگان</Form.Label>
            <Form.Select
              name={'datasetId'}
              className={'mb-1'}
              value={form.datasetId}
              disabled={true}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>

              <option value={form.datasetId}>
                {items.find(item => item?.dataset?.datasetId === form.datasetId)
                  ?.dataset?.shownName || '-'}
              </option>
            </Form.Select>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>دسته‌بندی درخواست</Form.Label>
            <Form.Select
              name={'categoryId'}
              className={'mb-1'}
              value={form.categoryId}
              disabled={true}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>
              <option value={0}>پرسش و پیشنهاد</option>
              <option value={1}>گزارش ایراد داده</option>
              <option value={2}>گزارش ایراد سیستم</option>
              <option value={3}>سایر</option>
            </Form.Select>
          </Form.Group>
        </Row>

        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <div className={'fs-6 text-gray-600 fmh-50'}>
            {form?.description || ''}
          </div>
        </Form.Group>

        <Form.Group className={'mb-3'}>
          <Form.Label>پاسخ به درخواست</Form.Label>
          <Form.Control
            as={'textarea'}
            rows={4}
            name={'response'}
            value={form.response}
            onChange={onHandleFormChange}
            placeholder={'لطفا پاسخ درخواست را وارد کنید'}
            disabled={loaddingUpdate || loadingData}
          />
        </Form.Group>
      </Form>
    ),
    delete: (
      <div>
        شما در حال حذف درخواست خود در دسته‌بندی
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.supportRequestId)
            )
            .map(item => MAP_STATUS[item?.categoryId] || '')
            .join(' و ')}
        </span>
        هستید.
      </div>
    ),
    view: (
      <div>
        <p className={'fs-6 text-soft-primary'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.supportRequestId)
            )
            .map(
              item => item?.response || 'پاسخی برای درخواست شما ثبت نشده است'
            )
            .join(' و ')}
        </p>
      </div>
    )
  }

  const onGenerateFooter = {
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onSubmitRequest}
          disabled={
            form.description.length === 0 ||
            form.categoryId.length === 0 ||
            loaddingAdd
          }
        >
          ثبت درخواست
        </Button>
      </>
    ),
    check: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onUpdateRequest}
          disabled={loaddingUpdate || loadingData}
        >
          ثبت بررسی درخواست
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'danger'}
          onClick={onDeleteRequest}
          disabled={loaddingDelete}
        >
          حذف درخواست
        </Button>
      </>
    ),
    view: (
      <Button variant={'soft-primary'} onClick={onCloseModal}>
        بستن
      </Button>
    )
  }

  const onGenerateActions = (
    <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
      {isAdmin ? (
        <>
          {selected.length > 0 && (
            <Button variant="primary" onClick={() => onShowModal('check')}>
              بررسی درخواست
            </Button>
          )}
        </>
      ) : (
        <>
          <Button variant="success" onClick={() => onShowModal('add')}>
            درخواست جدید
          </Button>
          {selected.length > 0 && (
            <>
              <Button variant={'primary'} onClick={() => onShowModal('view')}>
                مشاهده پاسخ
              </Button>

              {items
                .filter(item =>
                  selected
                    .map(sitem => JSON.parse(sitem.split('|')[0]))
                    .includes(item.supportRequestId)
                )
                .map(item => item?.stateId)[0] < 4 && (
                <Button
                  variant={'danger'}
                  onClick={() => onShowModal('delete')}
                >
                  حذف درخواست
                </Button>
              )}
            </>
          )}
        </>
      )}

      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
        <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
      </Modal>
    </div>
  )

  return (
    <Table
      items={items}
      loading={loadingItems}
      columns={SUPPORT.COLUMNS}
      checkedItems={selected}
      onChecked={onHandleSelected}
      isAdmin={isAdmin}
      actions={onGenerateActions}
      searchKey={'description'}
    />
  )
}
