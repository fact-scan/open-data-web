import { Button, Col, Form, Modal, Row } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { defaultFilter } from 'constants/defaults'
// import { Routes } from 'constants/routes'
import { USER } from 'constants/tables'
import useHttp from 'hooks/useHttp'
import React, { useCallback, useEffect, useState } from 'react'
// import { useHistory } from 'react-router-dom'

const DEFAULTS = {
  form: {
    firstName: '',
    lastName: '',
    emailaddress: '',
    mobileNumber: '',
    roleId: ''
  }
}

export default props => {
  // const { push } = useHistory()
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)
  const [items, setItems] = useState([])
  const [filter] = useState({ ...defaultFilter })
  const [form, setForm] = useState(DEFAULTS.form)
  const [roles, setRoles] = useState(null)
  const [loadingItems, responseItems, errorItems, onRequestItems] = useHttp()
  const [loaddingAdd, , , onRequestAdd] = useHttp()
  const [loaddingUpdate, , , onRequestUpdate] = useHttp()
  const [loaddingDelete, , , onRequestDelete] = useHttp()
  const [loaddingRole, , , onRequestRole] = useHttp()
  const [loadingData, , , onRequestData] = useHttp()

  const onGetItems = useCallback(() => {
    onRequestItems('GetAllUserInfoForAdmin', filter)
  }, [onRequestItems, filter])

  const onSubmitUser = async () => {
    const response = await onRequestAdd('AddUserInfo', form)

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onUpdateUser = async () => {
    const ids = selected[0].split('|')
    const userId = JSON.parse(ids[0])
    const response = await onRequestUpdate('UpdateUserInfo', {
      ...form,
      userId
    })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  const onDeleteUser = async () => {
    const ids = selected[0].split('|')
    const userId = JSON.parse(ids[0])
    const response = await onRequestDelete('DeleteUser', { userId })

    if (response?.isSuccess) {
      setSelected([])
      onCloseModal()
      onGetItems()
    }
  }

  useEffect(() => {
    onGetItems()
  }, [onGetItems])

  useEffect(() => {
    if (responseItems?.isSuccess) {
      setItems([
        ...responseItems?.value?.dataList.map(item => ({
          ...item,
          user: {
            ...item.user,
            firstName: item.firstName,
            lastName: item.lastName
          }
        }))
      ])
    }
    if (errorItems?.isFailed) {
      console.warn('Error: GetAllUserInfoForAdmin', errorItems)
    }
  }, [responseItems, errorItems])

  useEffect(() => {
    if ((isModal === 'add' || isModal === 'edit') && roles === null) {
      ;(async () => {
        const response = await onRequestRole('GetAllRole', filter)
        if (response?.value?.dataList?.length) {
          setRoles(response?.value?.dataList)
        }
      })()
    }

    if (isModal === 'edit') {
      ;(async () => {
        const ids = selected[0].split('|')
        const userId = JSON.parse(ids[0])
        const response = await onRequestData('GetUserInfo', { userId })
        if (response?.value) {
          setForm({
            firstName: response?.value?.firstName || '',
            lastName: response?.value?.lastName || '',
            emailaddress: response?.value?.emailaddress || '',
            mobileNumber: response?.value?.mobileNumber || '',
            roleId: response?.value?.roles.map(role => role?.roleId)[0] || '',
            userId
          })
        }
      })()
    }
    if (!isModal) {
      setForm(DEFAULTS.form)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModal])

  const onHandleFormChange = event => {
    const { name, value } = event.target
    const newForm = {
      normal: { ...form, [name]: value },
      roleId: { ...form, [name]: parseInt(value) }
    }
    setForm(newForm[name] || newForm['normal'])
  }

  const onHandleSelected = checkedItems => {
    setSelected(checkedItems)
  }

  // const onGoToUserRequest = () => {
  //   const ids = selected[0].split('|')
  //   const userId = JSON.parse(ids[0])
  //   push(Routes.Admin.UserRequest.redirect(userId))
  // }

  // const onGoToUserData = () => {
  //   const ids = selected[0].split('|')
  //   const userId = JSON.parse(ids[0])
  //   push(Routes.Admin.UserData.redirect(userId))
  // }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateTitle = {
    add: 'افزودن کاربر جدید',
    edit: 'ویرایش کاربر',
    delete: 'حذف کاربر'
  }

  const onGenerateBody = {
    add: (
      <Form>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>نام</Form.Label>
            <Form.Control
              type={'text'}
              name={'firstName'}
              value={form.firstName}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingAdd}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>نام خانوادگی</Form.Label>
            <Form.Control
              type={'text'}
              name={'lastName'}
              value={form.lastName}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام خانوادگی کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingAdd}
            />
          </Form.Group>
        </Row>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>شماره تماس</Form.Label>
            <Form.Control
              type={'text'}
              name={'mobileNumber'}
              value={form.mobileNumber}
              onChange={onHandleFormChange}
              placeholder={'لطفا شماره تماس کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingAdd}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>نقش کاربر</Form.Label>
            <Form.Select
              name={'roleId'}
              className={'mb-1'}
              value={form.roleId}
              onChange={onHandleFormChange}
              disabled={loaddingRole || loaddingAdd || roles?.length === 0}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>

              {roles?.map((option, index) => (
                <option value={option.roleId} key={`option_type_${index}`}>
                  {option.title}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
        </Row>
      </Form>
    ),
    edit: (
      <Form>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>نام</Form.Label>
            <Form.Control
              type={'text'}
              name={'firstName'}
              value={form.firstName}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingUpdate || loadingData}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>نام خانوادگی</Form.Label>
            <Form.Control
              type={'text'}
              name={'lastName'}
              value={form.lastName}
              onChange={onHandleFormChange}
              placeholder={'لطفا نام خانوادگی کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingUpdate || loadingData}
            />
          </Form.Group>
        </Row>
        <Row className={'mb-3'}>
          <Form.Group as={Col}>
            <Form.Label>شماره تماس</Form.Label>
            <Form.Control
              type={'text'}
              name={'mobileNumber'}
              value={form.mobileNumber}
              onChange={onHandleFormChange}
              placeholder={'لطفا شماره تماس کاربر را وارد کنید'}
              disabled={loaddingRole || loaddingUpdate || loadingData}
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>نقش کاربر</Form.Label>
            <Form.Select
              name={'roleId'}
              className={'mb-1'}
              value={form.roleId}
              onChange={onHandleFormChange}
              disabled={loaddingRole || loaddingUpdate || loadingData}
            >
              <option disabled value={''}>
                انتخاب کنید
              </option>

              {roles?.map((option, index) => (
                <option value={option.roleId} key={`option_type_${index}`}>
                  {option.title}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
        </Row>
      </Form>
    ),
    delete: (
      <div>
        شما در حال حذف کاربر
        <span className={'fw-medium text-danger d-inline-flex mx-2'}>
          {items
            .filter(item =>
              selected
                .map(sitem => JSON.parse(sitem.split('|')[0]))
                .includes(item.userId)
            )
            .map(item => `${item?.firstName || ''} ${item?.lastName || ''}`)
            .join(' و ')}
        </span>
        هستید.
      </div>
    )
  }

  const onGenerateFooter = {
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onSubmitUser}
          disabled={
            form.firstName.length === 0 ||
            form.lastName.length === 0 ||
            form.mobileNumber.length === 0 ||
            form.roleId.length === 0 ||
            loaddingAdd ||
            loaddingRole
          }
        >
          افزودن کاربر
        </Button>
      </>
    ),
    edit: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'success'}
          onClick={onUpdateUser}
          disabled={
            form.firstName.length === 0 ||
            form.lastName.length === 0 ||
            form.mobileNumber.length === 0 ||
            form.roleId.length === 0 ||
            loaddingUpdate ||
            loadingData ||
            loaddingRole
          }
        >
          ویرایش کاربر
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'soft-primary'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button
          variant={'danger'}
          onClick={onDeleteUser}
          disabled={loaddingDelete}
        >
          حذف کاربر
        </Button>
      </>
    )
  }

  const onGenerateActions = (
    <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
      {isAdmin && (
        <>
          <Button variant="success" onClick={() => onShowModal('add')}>
            افزودن کاربر
          </Button>
          {selected.length > 0 && (
            <>
              {/* {!isAlias && (
                <>
                  <Button variant="soft-primary" onClick={onGoToUserRequest}>
                    درخواست‌ها
                  </Button>

                  <Button variant="soft-primary" onClick={onGoToUserData}>
                    دسترسی‌ها
                  </Button>
                </>
              )} */}
              <Button variant="primary" onClick={() => onShowModal('edit')}>
                ویرایش
              </Button>
              <Button variant="danger" onClick={() => onShowModal('delete')}>
                حذف
              </Button>
            </>
          )}
        </>
      )}

      <Modal show={!!isModal} onHide={onCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
        <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
      </Modal>
    </div>
  )

  return (
    <Table
      items={items}
      loading={loadingItems}
      columns={USER.COLUMNS}
      checkedItems={selected}
      onChecked={onHandleSelected}
      isAdmin={isAdmin}
      actions={onGenerateActions}
      searchKey={'firstName'}
    />
  )
}
