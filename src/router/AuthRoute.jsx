import Preloader from 'components/Preloader'
import React, { lazy, Suspense } from 'react'
import { Redirect } from 'react-router-dom'

const AuthRoute = ({
  private: isPrivate,
  auth: isAuthenticated,
  page,
  container: Component,
  ...rest
}) => {
  const Page = page ? lazy(() => import(`pages/${page}`)) : <div />

  return (isAuthenticated && isPrivate) || (!isAuthenticated && !isPrivate) ? (
    <Component
      {...rest}
      component={props => (
        <Suspense fallback={<Preloader show={true} relative={true} />}>
          <Page {...props} isAdmin={rest.admin} isAlias={rest.alias} />
        </Suspense>
      )}
    />
  ) : isPrivate ? (
    <Redirect to={process.env.REACT_APP_AUTH_FAIL || '/login/'} />
  ) : (
    <Redirect to={process.env.REACT_APP_AUTH_SUCCESS || '/'} />
  )
}

export default AuthRoute
