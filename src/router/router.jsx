import Preloader from 'components/Preloader'
import { Routes } from 'constants/routes'
import LayoutLoader from 'layouts/LayoutLoader'
import LayoutNotFound from 'layouts/LayoutNotFound'
import LayoutSidebar from 'layouts/LayoutSidebar'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Switch } from 'react-router-dom'
import AuthRoute from 'router/AuthRoute'
import {
  GetUserAsync,
  resetUser,
  selectIsAdmin,
  selectToken,
  selectUser
} from 'store/slices/UserSlice'

const Router = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [currentRoutes, setCurrentRoutes] = useState(Object.values(Routes.User))
  const dispatch = useDispatch()
  const user = useSelector(selectUser)
  const token = useSelector(selectToken)
  const isAdmin = useSelector(selectIsAdmin)

  useEffect(() => {
    setIsLoading(true)
    dispatch(!token ? resetUser() : GetUserAsync())
  }, [token, dispatch])

  useEffect(() => {
    setCurrentRoutes(Object.values(Routes[isAdmin ? 'Admin' : 'User']))
  }, [isAdmin])

  useEffect(() => {
    setIsLoading(false)
  }, [user])

  return isLoading ? (
    <Preloader show={true} />
  ) : (
    <Switch>
      <AuthRoute
        exact
        container={LayoutLoader}
        {...Routes.Login}
        auth={!!user}
      />
      {currentRoutes.map(props => (
        <AuthRoute
          key={`routes_${isAdmin ? 'admin' : 'user'}`}
          container={LayoutSidebar}
          auth={!!user}
          {...props}
        />
      ))}
      <AuthRoute
        key={'routes_not_found'}
        container={LayoutNotFound}
        auth={!!user}
        path={'*'}
        private={true}
        title={'Not Found'}
        admin={false}
        sidebar={false}
        alias={false}
      />
    </Switch>
  )
}

export default Router
