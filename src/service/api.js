import * as Api from './endpoints'

const Endpoints = {
  ...Api.User,
  ...Api.UserManage,
  ...Api.Dataset,
  ...Api.DatasetPermission,
  ...Api.FeatureOperation,
  ...Api.Lookup,
  ...Api.Role,
  ...Api.SupportRequest,
  ...Api.Files
}

export default Endpoints
