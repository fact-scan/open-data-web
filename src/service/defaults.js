import axios from 'axios'
import Endpoints from './api'

export const InitialConfig = {
  baseURL: `${process.env.REACT_APP_REQ_BASEURL}/${process.env.REACT_APP_REQ_APIPATH}`,
  timeout: parseInt(process.env.REACT_APP_REQ_TIMEOUT || '30'),
  headers: {
    Accept: 'application/json'
  }
}

export const InitialError = {
  response: {
    data: {
      status: 'fail',
      code: '500',
      message: 'در حال حاضر ارتباط با سرور برقرار نیست'
    }
  }
}

export const PayloadType = {
  default: 'application/json;charset=UTF-8',
  formData: 'multipart/form-data;charset=UTF-8'
}

export const AsyncerEndpoint = async (
  endpoint,
  payload = {},
  token = '',
  isDeep = true
) => {
  const http = axios.create(InitialConfig)
  if (token) http.defaults.headers.common['Authorization'] = `Bearer ${token}`
  const method = Endpoints[endpoint].method
  const url = Endpoints[endpoint].url
  const responseType = Endpoints[endpoint].responseType
  const isFormData = Endpoints[endpoint].isFormData
  const payloadData = isFormData ? new FormData() : payload

  if (isFormData) {
    for (const key in payload) {
      payloadData.append(key, payload[key])
    }
  }

  http.defaults.headers.common['Content-Type'] =
    PayloadType[isFormData ? 'formData' : 'default']
  http.defaults.responseType = responseType

  const data =
    method.toLowerCase() === 'get' || method.toLowerCase() === 'delete'
      ? { params: payloadData }
      : { data: payloadData }

  try {
    const result = await http.request({ method, url, ...data })
    return isDeep ? await result.data : result
  } catch (error) {
    console.warn('Error: AsyncerEndpoint ', error)
    throw isDeep ? await error.response.data : error.response
  }
}

export const PayloadCreator = async (
  { endpoint, payload, isDeep },
  { getState, rejectWithValue }
) => {
  const token = getState().user.token || ''
  try {
    const response = await AsyncerEndpoint(endpoint, payload, token, isDeep)
    return response
  } catch (error) {
    throw rejectWithValue(error)
  }
}
