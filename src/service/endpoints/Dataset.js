const Dataset = {
  Test: {
    url: 'Dataset/Test',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddDataset: {
    url: 'Dataset/AddDataset',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetDataset: {
    url: 'Dataset/GetDataset',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateDataset: {
    url: 'Dataset/UpdateDataset',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  UpdateDatasetState: {
    url: 'Dataset/UpdateDatasetState',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  UpdateDatasetDates: {
    url: 'Dataset/UpdateDatasetDates',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteDataset: {
    url: 'Dataset/DeleteDataset',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetDatasetForAdmin: {
    url: 'Dataset/GetDatasetForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllDatasetForAdmin: {
    url: 'Dataset/GetAllDatasetForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllDataset: {
    url: 'Dataset/GetAllDataset',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddDatasetType: {
    url: 'Dataset/AddDatasetType',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetDatasetType: {
    url: 'Dataset/GetDatasetType',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateDatasetType: {
    url: 'Dataset/UpdateDatasetType',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteDatasetType: {
    url: 'Dataset/DeleteDatasetType',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetDatasetTypeForAdmin: {
    url: 'Dataset/GetDatasetTypeForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllDatasetTypeForAdmin: {
    url: 'Dataset/GetAllDatasetTypeForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllDatasetType: {
    url: 'Dataset/GetAllDatasetType',
    method: 'get',
    responseType: 'json',
    isFormData: false
  }
}

export default Dataset
