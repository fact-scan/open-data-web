const DatasetPermission = {
  AddPermissionRequest: {
    url: 'DatasetPermission/AddPermissionRequest',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetPermissionRequest: {
    url: 'DatasetPermission/GetPermissionRequest',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  ChangePermissionRequestState: {
    url: 'DatasetPermission/ChangePermissionRequestState',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  UpdatePermissionRequest: {
    url: 'DatasetPermission/UpdatePermissionRequest',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeletePermissionRequest: {
    url: 'DatasetPermission/DeletePermissionRequest',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetPermissionRequestForAdmin: {
    url: 'DatasetPermission/GetPermissionRequestForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllPermissionRequestForMe: {
    url: 'DatasetPermission/GetAllPermissionRequestForMe',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllPermissionRequestForAdmin: {
    url: 'DatasetPermission/GetAllPermissionRequestForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddRellUserDatasetTypePermission: {
    url: 'DatasetPermission/AddRellUserDatasetTypePermission',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetRellUserDatasetTypePermission: {
    url: 'DatasetPermission/GetRellUserDatasetTypePermission',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateRellUserDatasetTypePermission: {
    url: 'DatasetPermission/UpdateRellUserDatasetTypePermission',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteRellUserDatasetTypePermission: {
    url: 'DatasetPermission/DeleteRellUserDatasetTypePermission',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetRellUserDatasetTypePermissionForAdmin: {
    url: 'DatasetPermission/GetRellUserDatasetTypePermissionForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRellUserDatasetTypePermissionForMe: {
    url: 'DatasetPermission/GetAllRellUserDatasetTypePermissionForMe',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRellUserDatasetTypePermissionForAdmin: {
    url: 'DatasetPermission/GetAllRellUserDatasetTypePermissionForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetRellUserDatasetPermissionForAdmin: {
    url: 'DatasetPermission/GetRellUserDatasetPermissionForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRellUserDatasetPermissionForMe: {
    url: 'DatasetPermission/GetAllRellUserDatasetPermissionForMe',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRellUserDatasetPermissionForAdmin: {
    url: 'DatasetPermission/GetAllRellUserDatasetPermissionForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  }
}

export default DatasetPermission
