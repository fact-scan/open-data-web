const FeatureOperation = {
  AddFeature: {
    url: 'FeatureOperation/AddFeature',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetFeature: {
    url: 'FeatureOperation/GetFeature',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateFeature: {
    url: 'FeatureOperation/UpdateFeature',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteFeature: {
    url: 'FeatureOperation/DeleteFeature',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetFeatureForAdmin: {
    url: 'FeatureOperation/GetFeatureForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllFeatureForAdmin: {
    url: 'FeatureOperation/GetAllFeatureForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddOperation: {
    url: 'FeatureOperation/AddOperation',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetOperation: {
    url: 'FeatureOperation/GetOperation',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateOperation: {
    url: 'FeatureOperation/UpdateOperation',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteOperation: {
    url: 'FeatureOperation/DeleteOperation',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetOperationForAdmin: {
    url: 'FeatureOperation/GetOperationForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllOperationForAdmin: {
    url: 'FeatureOperation/GetAllOperationForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddRellFeatureOperation: {
    url: 'FeatureOperation/AddRellFeatureOperation',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  AddOrUpdateRellFeatureOperation: {
    url: 'FeatureOperation/AddOrUpdateRellFeatureOperation',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetRellFeatureOperation: {
    url: 'FeatureOperation/GetRellFeatureOperation',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateRellFeatureOperation: {
    url: 'FeatureOperation/UpdateRellFeatureOperation',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteRellFeatureOperation: {
    url: 'FeatureOperation/DeleteRellFeatureOperation',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetRellFeatureOperationForAdmin: {
    url: 'FeatureOperation/GetRellFeatureOperationForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRellFeatureOperationForAdmin: {
    url: 'FeatureOperation/GetAllRellFeatureOperationForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  }
}

export default FeatureOperation
