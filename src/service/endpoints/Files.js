const Files = {
  UploadFile: {
    url: 'Files/UploadFile',
    method: 'post',
    responseType: 'json',
    isFormData: true
  },
  getFile: {
    url: 'Files/getFile',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  getFileBase64Type: {
    url: 'Files/getFileBase64Type/<<id>>',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  DeleteFile: {
    url: 'Files/DeleteFile',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  UpdateFile: {
    url: 'Files/UpdateFile',
    method: 'put',
    responseType: 'json',
    isFormData: false
  }
}

export default Files
