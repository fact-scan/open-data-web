export const Lookup = {
  AddLookup: {
    url: 'Lookup/AddLookup',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetLookup: {
    url: 'Lookup/GetLookup',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllLookup: {
    url: 'Lookup/GetAllLookup',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  SearchLookupByType: {
    url: 'Lookup/SearchLookupByType',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateLookup: {
    url: 'Lookup/UpdateLookup',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteLookup: {
    url: 'Lookup/DeleteLookup',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  }
}

export default Lookup
