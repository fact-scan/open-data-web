const Role = {
  AddRole: {
    url: 'Role/AddRole',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  DeleteRole: {
    url: 'Role/DeleteRole',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetRole: {
    url: 'Role/GetRole',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllRole: {
    url: 'Role/GetAllRole',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateRole: {
    url: 'Role/UpdateRole',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  AddRellUserRole: {
    url: 'Role/AddRellUserRole',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  DeleteRellUserRole: {
    url: 'Role/DeleteRellUserRole',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetUserRoles: {
    url: 'Role/GetUserRoles',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllPermissions: {
    url: 'Role/GetAllPermissions',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetPermissionRoles: {
    url: 'Role/GetPermissionRoles',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetPermissionRolesByTitle: {
    url: 'Role/GetPermissionRolesByTitle',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddRellPermissionRole: {
    url: 'Role/AddRellPermissionRole',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  AddAndUpdateRangeRellPermissionRole: {
    url: 'Role/AddAndUpdateRangeRellPermissionRole',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  DeleteRellPermissionRole: {
    url: 'Role/DeleteRellPermissionRole',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  }
}

export default Role
