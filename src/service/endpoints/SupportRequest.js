const SupportRequest = {
  AddSupportRequest: {
    url: 'SupportRequest/AddSupportRequest',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetSupportRequest: {
    url: 'SupportRequest/GetSupportRequest',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  UpdateSupportRequest: {
    url: 'SupportRequest/UpdateSupportRequest',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  DeleteSupportRequest: {
    url: 'SupportRequest/DeleteSupportRequest',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  },
  GetSupportRequestForAdmin: {
    url: 'SupportRequest/GetSupportRequestForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllSupportRequestForAdmin: {
    url: 'SupportRequest/GetAllSupportRequestForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetAllSupportRequestForMe: {
    url: 'SupportRequest/GetAllSupportRequestForMe',
    method: 'get',
    responseType: 'json',
    isFormData: false
  }
}

export default SupportRequest
