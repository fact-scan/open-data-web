const User = {
  Test: {
    url: 'User/Test',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AuthWithMobileNumber: {
    url: 'User/AuthWithMobileNumber',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  VerifyAuthWithMobileNumber: {
    url: 'User/VerifyAuthWithMobileNumber',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  AddUserInfoMe: {
    url: 'User/AddUserInfo',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  UpdateUserInfoMe: {
    url: 'User/UpdateUserInfo',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  GetUserInfoMe: {
    url: 'User/GetUserInfo',
    method: 'get',
    responseType: 'json',
    isFormData: false
  }
}

export default User
