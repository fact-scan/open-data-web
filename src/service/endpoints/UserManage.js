const UserManage = {
  AddUser: {
    url: 'UserManage/AddUser',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  GetUser: {
    url: 'UserManage/GetUser',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  GetUserInfo: {
    url: 'UserManage/GetUserInfo',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  AddUserInfo: {
    url: 'UserManage/AddUserInfo',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  UpdateUserInfo: {
    url: 'UserManage/UpdateUserInfo',
    method: 'put',
    responseType: 'json',
    isFormData: false
  },
  GetAllUserInfoForAdmin: {
    url: 'UserManage/GetAllUserInfoForAdmin',
    method: 'get',
    responseType: 'json',
    isFormData: false
  },
  DeleteUser: {
    url: 'UserManage/DeleteUser',
    method: 'delete',
    responseType: 'json',
    isFormData: false
  }
}

export default UserManage
