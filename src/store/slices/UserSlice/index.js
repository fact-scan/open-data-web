import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { PayloadCreator } from 'service/defaults'

const initialState = {
  token: '',
  data: undefined,
  isAdmin: false
}

export const GetUserAsync = createAsyncThunk('user/GetUser', (_, thunkAPI) =>
  PayloadCreator({ endpoint: 'GetUserInfoMe' }, thunkAPI)
)

export const UserSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload
    },
    setUser: (state, action) => {
      state.data = action.payload
    },
    setIsAdmin: (state, action) => {
      state.isAdmin = action.payload
    },
    resetUser: state => {
      state.token = ''
      state.data = undefined
      state.isAdmin = false
    }
  },
  extraReducers: builder => {
    builder
      .addCase(GetUserAsync.fulfilled, (state, action) => {
        state.data = action.payload?.value
      })
      .addCase(GetUserAsync.rejected, (state, action) => {
        console.warn('Error: GetUserAsync', action.payload)
        state.token = ''
        state.data = undefined
        state.isAdmin = false
      })
  }
})

export const selectToken = state => state.user.token
export const selectUser = state => state.user.data
export const selectIsAdmin = state => state.user.isAdmin

export const { setToken, setUser, setIsAdmin, resetUser } = UserSlice.actions

export default UserSlice.reducer
